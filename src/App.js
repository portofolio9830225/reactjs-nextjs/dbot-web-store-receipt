import React, { Component } from "react";
import { Switch } from "react-router-dom";
import PublicRoute from "./components/layout/PublicRoute";

import PageNotFound from "./components/NotFound";
import Receipt from "./pages/Receipt";

class App extends Component {
  render() {
    return (
      <div id="HOC">
        <Switch>
          <PublicRoute exact path="/:oID" component={Receipt} />

          <PublicRoute component={PageNotFound} />
        </Switch>
      </div>
    );
  }
}

export default App;
