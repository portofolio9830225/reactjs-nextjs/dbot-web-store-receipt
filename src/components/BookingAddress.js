import React, { Component } from "react";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "next/router";
import isEmpty from "../utils/isEmpty";
import {
  withMobileDialog,
  Typography,
  MenuItem,
  Collapse,
  Fade,
} from "@material-ui/core";

import {
  customNotification,
  clearNotification,
} from "../store/actions/notificationAction";

import { greydark, grey, mainBgColor } from "../utils/ColorPicker";
import TextInput from "./mini/TextInput";
import stateList from "../utils/stateList";

const styles = (theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
    position: "relative",
  },
  image: {
    width: "100%",
    height: "100%",
    objectFit: "cover",
  },
  main: {
    alignSelf: "center",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    width: "100%",
    //marginBottom: "3rem",
  },
  productCard: {
    marginTop: "2rem",
    width: "100%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    borderRadius: "15px",
    boxShadow: "5px 5px 8px #e3eeff, -5px -5px 5px #ffffff",
    overflow: "hidden",
    backgroundColor: mainBgColor,
  },
  productCardImgContainer: {
    position: "relative",
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
    height: "100%",
    maxWidth: "500px",
    maxHeight: "500px",
  },

  productCardTextContainer: {
    width: "100%",
    boxSizing: "border-box",
    padding: "1rem 0",
    display: "flex",
    flexDirection: "column",
  },
  productCardActionContainer: {
    marginTop: "0.5rem",
    width: "100%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },

  title: {
    fontSize: "2.3rem",
    fontWeight: 500,
    letterSpacing: "0.5px",
    width: "100%",
  },
  lDescTitle: {
    fontWeight: 700,
    marginTop: "1rem",
    width: "100%",
  },

  lDescText: {
    fontSize: "1.3rem",
    marginTop: "0.5rem",
    width: "100%",
    fontWeight: 300,
    letterSpacing: "0.2px",
    padding: 0,
  },
  price: {
    fontSize: "1.7rem",
    color: greydark,
  },

  counterClickerContainer: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    padding: "0.3rem",
    boxSizing: "border-box",
    alignItems: "center",
    width: "30%",
    minWidth: "125px",
    // background: "#e0e0e0",
    boxShadow: "inset 2px 2px 4px #d6d6e3, inset -2px -2px 4px #ffffff",
    height: "calc(40px + 0.8rem)",
    borderRadius: "calc(20px + 0.4rem)",
  },
  stockText: {
    alignSelf: "flex-end",
    fontWeight: 300,
    fontSize: "1rem",
    color: grey,
    paddingRight: "0.5rem",
    paddingTop: "0.3rem",
  },
  counterText: {
    fontSize: "1.2rem",
    color: greydark,
    fontWeight: "bold",
    textShadow: "1px 1px 2px #999, -1px -1px 2px #ffffff",
  },

  counterIconContainer: {
    transition: "all 0.3s",
    cursor: "pointer",

    boxShadow:
      "-5px -5px 5px rgba(255, 255, 255, 0.5), 5px 5px 10px rgba(174, 174, 192, 0.5), inset -2px -2px 4px rgba(0, 0, 0, 0.1), inset 2px 2px 4px #FFFFFF",
    // boxShadow: "2px 2px 4px #e3eeff, -2px -2px 4px #ffffff",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    height: "40px",
    width: "40px",
    borderRadius: "20px",
    background: "linear-gradient(145deg, #fcfdff, #dbdbe8)",
    "&:active": {
      background: "linear-gradient(145deg, #dbdbe8, #fcfdff)",
    },
  },
  counterIcon: {
    color: greydark,
    fontSize: "1.3rem",
    fontWeight: "bold",
  },
  button: {
    width: "100%",
  },
  dividerGrey: {
    width: "100%",
    borderBottom: "0.5px solid gainsboro",
  },

  textField: {
    width: "100%",
    marginTop: "1.5rem",
  },
  orderCard: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    position: "fixed",
    bottom: 0,
    left: 0,
    width: "100%",
    zIndex: 100,
  },
  orderCardBackground: {
    display: "flex",
    width: "100%",
    maxWidth: "500px",
    backgroundColor: mainBgColor,
    boxShadow: "0px -2px 2px rgba(0,0,0,0.03)",
  },
  orderCardContent: {
    display: "flex",
    flexDirection: "column",
    margin: "auto",
    width: "100%",
    padding: "1rem 0",
    [theme.breakpoints.down("xs")]: {
      width: "90%",
    },
  },
  priceList: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%",
    marginBottom: "1rem",
  },
  DialogRoot: {
    backgroundColor: "rgba(0,0,0,0)",
    boxShadow: "none",
    display: "flex",
    borderRadius: 0,
  },
});

class BookingAddress extends Component {
  state = {
    sZone: "",
    address1: "",
    address2: "",
    address3: "",
    address4: "",
    postcode: "",
    staName: "",
    ciName: "",
  };

  componentDidMount() {
    // if (!isEmpty(this.props.booking.bookDetails)) {
    //   let {
    //     address_1,
    //     address_2,
    //     address_3,
    //     address_4,
    //     postcode,
    //     city,
    //     state,
    //   } = this.props.booking.bookDetails.addressData;

    //   this.setState(
    //     {
    //       address1: !isEmpty(address_1) ? address_1 : "",
    //       address2: !isEmpty(address_2) ? address_2 : "",
    //       address3: !isEmpty(address_3) ? address_3 : "",
    //       address4: !isEmpty(address_4) ? address_4 : "",
    //       ciName: !isEmpty(city) ? city : "",
    //       staName: !isEmpty(state) ? state : "",
    //       postcode: !isEmpty(postcode) ? postcode : "",
    //       sZone: this.props.sZone,
    //     },
    //     () => {
    //       this.handleChange();
    //     }
    //   );
    // } else {
    this.setState(
      {
        sZone: !isEmpty(this.props.sZone) ? this.props.sZone : "",
        address1: !isEmpty(this.props.address1) ? this.props.address1 : "",
        address2: !isEmpty(this.props.address2) ? this.props.address2 : "",
        address3: !isEmpty(this.props.address3) ? this.props.address3 : "",
        address4: !isEmpty(this.props.address4) ? this.props.address4 : "",
        ciName: !isEmpty(this.props.ciName) ? this.props.ciName : "",
        staName: !isEmpty(this.props.staName) ? this.props.staName : "",
        postcode: !isEmpty(this.props.postcode) ? this.props.postcode : "",
      },
      () => {
        this.handleChange();
      }
    );
    // }
  }

  handleChange = () => {
    let {
      sZone,
      address1,
      address2,
      address3,
      address4,
      postcode,
      staName,
      ciName,
    } = this.state;
    this.props.onChange({
      sZone,
      address1,
      address2,
      address3,
      address4,
      postcode,
      staName,
      ciName,
    });
  };

  handleText =
    (name, limit = 999) =>
    (event) => {
      let val = event.target.value;
      if (val.length <= limit) {
        this.setState({ [name]: val }, () => {
          this.handleChange();
        });
      }
    };

  handleBack = () => {
    this.props.onClose();
  };

  render() {
    const { classes, isProduct, isWeightBased, fullScreen } = this.props;
    const {
      sZone,
      address1,
      address2,
      address3,
      address4,
      postcode,
      staName,
      ciName,
      pMethod,
    } = this.state;
    const { shipping } = this.props.cart.shipping;

    if (!isEmpty(shipping)) {
      return (
        <div className={classes.root}>
          <Fade in={true}>
            <div className={classes.main}>
              <Collapse
                in={!isEmpty(shipping.zone) || !isEmpty(shipping.product)}
              >
                {!isWeightBased ? (
                  <TextInput
                    select
                    className={classes.textField}
                    variant="outlined"
                    label="Shipping zone"
                    value={sZone}
                    onChange={this.handleText("sZone")}
                    errorkey="zone_number"
                  >
                    <MenuItem value="" disabled>
                      Select shipping zone
                    </MenuItem>
                    {isProduct
                      ? shipping.product.map((e, i) => {
                          return (
                            <MenuItem key={i} value={`${e.number}`}>
                              <div
                                style={{
                                  width: "100%",
                                  display: "flex",
                                  flexDirection: "row",
                                  justifyContent: "space-between",
                                }}
                              >
                                <Typography> {e.name}</Typography>
                                <Typography></Typography>
                              </div>
                            </MenuItem>
                          );
                        })
                      : shipping.zone.map((e, i) => {
                          return (
                            <MenuItem key={i} value={`${e.number}`}>
                              <div
                                style={{
                                  width: "100%",
                                  display: "flex",
                                  flexDirection: "row",
                                  justifyContent: "space-between",
                                }}
                              >
                                <Typography> {e.name}</Typography>
                                <Typography>RM{e.price.toFixed(2)}</Typography>
                              </div>
                            </MenuItem>
                          );
                        })}
                  </TextInput>
                ) : (
                  <div />
                )}
              </Collapse>
              <TextInput
                className={classes.textField}
                variant="outlined"
                label="Address line 1"
                value={address1}
                onChange={this.handleText("address1", 35)}
                errorkey="address_1"
              />
              <TextInput
                className={classes.textField}
                variant="outlined"
                label="Address line 2 (Optional)"
                placeholder="Optional"
                value={address2}
                onChange={this.handleText("address2", 35)}
                errorkey="address_2"
              />
              <Collapse in={address2.length >= 2}>
                <TextInput
                  className={classes.textField}
                  variant="outlined"
                  label="Address line 3 (Optional)"
                  placeholder="Optional"
                  value={address3}
                  onChange={this.handleText("address3", 35)}
                  errorkey="address_3"
                />
              </Collapse>
              <Collapse in={address3.length >= 2}>
                <TextInput
                  className={classes.textField}
                  variant="outlined"
                  label="Address line 4 (Optional)"
                  placeholder="Optional"
                  value={address4}
                  onChange={this.handleText("address4", 35)}
                  errorkey="address_4"
                />
              </Collapse>
              <div
                style={{
                  width: "100%",
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "space-between",
                }}
              >
                <div style={{ width: "35%" }}>
                  <TextInput
                    className={classes.textField}
                    variant="outlined"
                    label="Postcode"
                    type="number"
                    value={postcode}
                    onChange={this.handleText("postcode", 5)}
                    errorkey="postcode"
                  />
                </div>
                <div style={{ width: "63%" }}>
                  <TextInput
                    className={classes.textField}
                    variant="outlined"
                    label="City"
                    value={ciName}
                    onChange={this.handleText("ciName")}
                    errorkey="city"
                  />
                </div>
              </div>

              <TextInput
                select
                className={classes.textField}
                variant="outlined"
                label="State"
                value={staName}
                onChange={this.handleText("staName")}
                errorkey="state"
              >
                <MenuItem value="" disabled>
                  Select state
                </MenuItem>
                {stateList.map((e, i) => {
                  return (
                    <MenuItem key={i} value={e}>
                      {e}
                    </MenuItem>
                  );
                })}
              </TextInput>
            </div>
          </Fade>
        </div>
      );
    } else {
      return null;
    }
  }
}

const mapStateToProps = (state) => ({
  item: state.item,
  booking: state.booking,
  business: state.business,
  cart: state.cart,
  customer: state.customer,
  loading: state.loading,
  notification: state.notification,
  error: state.error,
});

export default connect(mapStateToProps, {
  customNotification,
  clearNotification,
})(
  withStyles(styles)(
    withMobileDialog({ breakpoint: "xs" })(withRouter(BookingAddress))
  )
);
