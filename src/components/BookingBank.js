import React, { Component } from "react";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { withRouter } from "next/router";
import isEmpty from "../utils/isEmpty";
import { withMobileDialog, MenuItem, Fade, Collapse } from "@material-ui/core";

import {
  customNotification,
  clearNotification,
} from "../store/actions/notificationAction";

import TextInput from "./mini/TextInput";
import BankBox from "./mini/BankBox";
import { getPaymentMethod } from "../store/actions/cartAction";

const styles = (theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
    position: "relative",
  },
  main: {
    alignSelf: "center",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    width: "100%",
    marginBottom: "3rem",
  },
  textField: {
    width: "100%",
    marginTop: "1.5rem",
  },

  BankMenuItem: {
    paddingTop: "1.5rem",
    paddingBottom: "1.5rem",
  },
});

class BookingBank extends Component {
  state = {
    bank: "",
    pMethod: "",
    note: "",
  };

  componentDidMount() {
    this.setState(
      {
        bank: !isEmpty(this.props.bank) ? this.props.bank : "",
        pMethod: !isEmpty(this.props.pMethod) ? this.props.pMethod : "",
        note: !isEmpty(this.props.note) ? this.props.note : "",
      },
      () => {
        this.handleChange();
      }
    );
  }

  handleChange = () => {
    let { bank, pMethod, note } = this.state;
    this.props.onChange({
      bank,
      pMethod,
      note,
    });
  };

  handleText =
    (name, limit = 999) =>
    (event) => {
      let val = event.target.value;

      this.setState({ [name]: val }, () => {
        this.handleChange();
      });
    };

  handleBack = () => {
    this.props.onClose();
  };

  render() {
    const { classes, isLockPaymentType } = this.props;
    const { bank, pMethod, note } = this.state;
    const { paymentMethod } = this.props.cart;
    const { fpx } = this.props.booking;

    return (
      <div className={classes.root}>
        <Fade in={true}>
          <div className={classes.main}>
            <TextInput
              multiline
              rows={3}
              className={classes.textField}
              variant="outlined"
              label="Note to seller (optional)"
              placeholder="Optional"
              value={note}
              onChange={this.handleText("note")}
            />
            <TextInput
              select
              disabled={isLockPaymentType}
              className={classes.textField}
              variant="outlined"
              label="Payment type"
              value={pMethod}
              onChange={this.handleText("pMethod")}
              errorkey="payment_method"
            >
              <MenuItem value="" disabled>
                Select payment type
              </MenuItem>

              {paymentMethod.map((e, i) => {
                return (
                  <MenuItem key={i} value={e.payment_method_id}>
                    {e.method_name}
                  </MenuItem>
                );
              })}
            </TextInput>
            <Collapse in={pMethod == 1}>
              <TextInput
                select
                disabled={isEmpty(fpx)}
                className={classes.textField}
                variant="outlined"
                label="Online banking"
                value={bank}
                onChange={this.handleText("bank")}
                errorkey="bank_code"
              >
                <MenuItem value="" disabled>
                  Select bank
                </MenuItem>

                {fpx.map((e, i) => {
                  return (
                    <MenuItem
                      key={i}
                      classes={{ root: classes.BankMenuItem }}
                      value={e.code}
                      disabled={!e.status}
                    >
                      <BankBox img={e.image} name={e.name} status={e.status} />
                    </MenuItem>
                  );
                })}
              </TextInput>
            </Collapse>
          </div>
        </Fade>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  booking: state.booking,
  cart: state.cart,
  notification: state.notification,
  error: state.error,
});

export default connect(mapStateToProps, {
  getPaymentMethod,
  customNotification,
  clearNotification,
})(
  withStyles(styles)(
    withMobileDialog({ breakpoint: "xs" })(withRouter(BookingBank))
  )
);
