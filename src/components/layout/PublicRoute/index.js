import React from "react";
import { Route } from "react-router-dom";
import { connect } from "react-redux";

import Layout from "./component";
// import ScrollToTop from "../../../utils/ScrollToTop";

const PublicRoute = ({
  component: Component,
  render,
  noNavbar = false,
  content,
  page,
  ...rest
}) => {
  return (
    <Layout noNavbar={noNavbar}>
      <Route
        {...rest}
        render={
          render
            ? render
            : (props) => {
                return (
                  // <ScrollToTop>
                  <Component {...props} content={content} page={page} />
                  // </ScrollToTop>
                );
              }
        }
      />
    </Layout>
  );
};

export default PublicRoute;
