import { Typography, withStyles } from "@material-ui/core";
import React from "react";

const styles = (theme) => ({
  bankBox: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
  },
  bankBoxImg: {
    borderRadius: "50%",
    overflow: "hidden",
    width: "20px",
    height: "20px",
    // [theme.breakpoints.down("xs")]: {
    //   width: "30px",
    //   height: "30px",
    // },
  },
  bankBoxText: {
    fontSize: "1rem",
    marginLeft: "1rem",
    [theme.breakpoints.down("xs")]: {
      fontSize: "0.9rem",
    },
  },
});

class BankBox extends React.Component {
  render() {
    const { classes, img, name, status } = this.props;
    return (
      <div className={classes.bankBox}>
        <div className={classes.bankBoxImg}>
          <img width="100%" src={img} alt={img} />
        </div>
        <Typography className={classes.bankBoxText}>
          {name}
          {!status ? " (Offline)" : ""}
        </Typography>
      </div>
    );
  }
}

export default withStyles(styles)(BankBox);
