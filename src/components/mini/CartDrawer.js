import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "next/router";
import { withStyles } from "@material-ui/core/styles";
import isEmpty from "../../utils/isEmpty";
import CloseIcon from "@material-ui/icons/CloseRounded";
import {
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
  Paper,
  Typography,
  withMobileDialog,
} from "@material-ui/core";

import { customNotification } from "../../store/actions/notificationAction";
import { black, secondary, secondaryDark } from "../../utils/ColorPicker";
import getDuration from "../../utils/getDuration";
import DateConverter from "../../utils/DateConverter";

const styles = (theme) => ({
  root: {
    position: "relative",
    width: "100%",
    minHeight: "100vh",
    display: "flex",
    boxSizing: "border-box",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "space-between",
  },
  container: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
    height: "100%",
  },
  image: {
    width: "100%",
  },
  topDrawer: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    backgroundColor: black,
    padding: "0 0 2rem",
    marginBottom: "1rem",
  },
  topDrawerContent: {
    width: "100%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-evenly",
  },
  cartListBox: {
    width: "100%",
    marginBottom: "3rem",
    padding: "0 2rem",
    boxSizing: "border-box",
    display: "flex",
    flexDirection: "column",
  },
  itemList: {
    width: "100%",
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    padding: "1rem 0",
  },
  itemListImgContainer: {
    width: "15%",
  },
  itemListContent: {
    width: "80%",
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  itemListContentLeft: {
    display: "flex",
    flexDirection: "column",
  },
  itemListContentTitle: {
    fontSize: "1.3rem",
    fontWeight: 500,
  },
  itemListContentColor: {
    fontSize: "1.1rem",
    fontWeight: 300,
  },
  itemListContentCounterBox: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
  },
  itemListContentCounterIcon: {
    cursor: "pointer",
    fontSize: "1.5rem",
  },
  itemListContentCounterText: {
    fontSize: "1.3rem",
    margin: "0 1rem",
    color: "dimgrey",
  },
  itemListContentRight: {
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-end",
  },
  itemListContentPrice: {
    fontSize: "1.2rem",
  },
  itemListContentRemoveButton: {
    fontSize: "0.9rem",
    color: secondaryDark,
    fontWeight: 500,
    paddingTop: "0.5rem",
    cursor: "pointer",
    textDecoration: "underline",
  },
  titleDesc: {
    display: "flex",
    flexDirection: "column",
    marginBottom: "1rem",
  },
  titleDescTitle: {
    fontSize: "1rem",
    color: "grey",
  },
  titleDescDesc: {
    fontSize: "1.1rem",
    fontWeight: 500,
  },
  breakdownBox: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    padding: "1rem 2rem 0 2rem",
    boxSizing: "border-box",
  },
  cartListEmptyBox: {
    height: "90%",
    display: "flex",
    alignSelf: "center",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  cartListEmptyText: {
    fontSize: "1.3rem",
    marginBottom: "0.5rem",
  },
  continueShopButton: {
    color: secondary,
    textDecoration: "underline",
    fontSize: "1.2rem",
    cursor: "pointer",
    fontWeight: 500,
    paddingBottom: "1rem",
    paddingTop: "0.5rem",
    textAlign: "center",
    [theme.breakpoints.down("xs")]: {
      fontSize: "1.1rem",
    },
  },
  minLimitBox: {
    width: "100%",
    backgroundColor: secondaryDark,
  },
  minLimitText: {
    color: "white",
    padding: "0.5rem 1rem",
    boxSizing: "border-box",
    textAlign: "center",
  },
  priceList: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    margin: "0.5rem",
  },

  priceListText: {
    fontSize: "1.5rem",
    fontWeight: 700,
  },

  checkoutButton: {
    alignSelf: "center",
    margin: "0 0 1rem",
    height: "50px",
    borderRadius: "10px",
    width: "100%",
  },
  continueText: {
    fontSize: "1.2rem",
    padding: "1rem",
    textAlign: "center",
    textDecoration: "underline",
    fontWeight: 500,
    cursor: "pointer",
    textTransform: "uppercase",
  },
  closeIcon: {
    alignSelf: "flex-start",
    color: "white",
    padding: "1rem",
    fontSize: "2.5rem",
    margin: "1rem 0",
    cursor: "pointer",
  },
  dateHolder: {
    display: "flex",
    flexDirection: "column",
    width: "30%",
  },
  dateHolderTitle: {
    fontSize: "1rem",
    color: "white",
    [theme.breakpoints.down("xs")]: {
      fontSize: "0.8rem",
    },
  },
  dateHolderContent: {
    backgroundColor: "white",
    padding: "0.8rem 0.5rem",
    borderRadius: "7px",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  dateHolderText: {
    fontSize: "1.3rem",
    [theme.breakpoints.down("xs")]: {
      fontSize: "0.9rem",
    },
  },
});

class CartDrawer extends Component {
  state = {
    item: {},
  };

  handleRemoveDialog =
    (item = {}) =>
    () => {
      this.setState({
        item,
      });
    };

  handleRemoveProduct = (item) => () => {
    this.handleRemoveDialog()();
    // this.props.deleteCartItem(this.props.item.cart[0].cart_id, item.item_id);
  };

  handlePage = (page) => () => {
    this.props.router.push(page);
    this.props.onClose();
  };

  render() {
    const { classes, fullScreen, onClose } = this.props;
    const { item } = this.state;
    const { cart } = this.props.item;

    const ItemList = (props) => (
      <div
        className={classes.itemList}
        style={!props.first ? { borderTop: "1px solid gainsboro" } : {}}
      >
        <Paper className={classes.itemListImgContainer}>
          <img src={props.img} alt={props.img} className={classes.image} />
        </Paper>
        <div className={classes.itemListContent}>
          <div className={classes.itemListContentLeft}>
            <Typography className={classes.itemListContentTitle}>
              {props.title}
            </Typography>
            <Typography
              onClick={props.onRemove}
              className={classes.itemListContentRemoveButton}
            >
              Remove
            </Typography>
          </div>
          <div className={classes.itemListContentRight}>
            <Typography className={classes.itemListContentPrice}>
              RM{props.price}
            </Typography>
          </div>
        </div>
      </div>
    );

    const DateHolder = (props) => {
      return (
        <div className={classes.dateHolder}>
          <Typography className={classes.dateHolderTitle}>
            {props.title}
          </Typography>
          <div className={classes.dateHolderContent}>
            <Typography className={classes.dateHolderText}>
              {props.text}
            </Typography>
          </div>
        </div>
      );
    };

    const PriceList = (props) => {
      return (
        <div className={classes.priceList}>
          <Typography className={classes.priceListText}>
            {props.title}
          </Typography>

          <Typography className={classes.priceListText}>
            RM{props.price}
          </Typography>
        </div>
      );
    };

    const TitleDesc = (props) => (
      <div className={classes.titleDesc}>
        <Typography className={classes.titleDescTitle}>
          {props.title}
        </Typography>
        <Typography className={classes.titleDescDesc}>{props.desc}</Typography>
      </div>
    );

    const CloseButton = (props) => {
      return (
        <CloseIcon
          style={props.inverted ? { color: "black" } : {}}
          className={classes.closeIcon}
          onClick={onClose}
        />
      );
    };

    const DurationText = (props) => {
      let d = getDuration(props.start, props.end);
      let a = d > 1 ? "days" : "day";
      return `${d} ${a}`;
    };

    return (
      <div className={classes.root}>
        <div className={classes.container}>
          {!isEmpty(cart[0]) ? (
            <div className={classes.topDrawer}>
              <CloseButton />
              <div className={classes.topDrawerContent}>
                <DateHolder
                  title="Start date"
                  text={DateConverter(cart[0].start_date)}
                />
                <DateHolder
                  title="End date"
                  text={DateConverter(cart[0].end_date)}
                />
                <DateHolder
                  title="Duration"
                  text={
                    <DurationText
                      start={cart[0].start_date}
                      end={cart[0].end_date}
                    />
                  }
                />
              </div>
            </div>
          ) : (
            <CloseButton inverted />
          )}
          <Dialog open={!isEmpty(item)} fullScreen={fullScreen}>
            <DialogTitle>Remove {item.item_title}?</DialogTitle>
            <DialogActions>
              <Button onClick={this.handleRemoveDialog()}>cancel</Button>
              <Button
                onClick={this.handleRemoveProduct(item)}
                style={{ color: "firebrick" }}
              >
                remove
              </Button>
            </DialogActions>
          </Dialog>
          {!isEmpty(cart[0]) ? (
            <div className={classes.cartListBox}>
              {cart[0].items.map((e, i) => {
                return (
                  <ItemList
                    key={i}
                    first={i === 0}
                    title={e.item_title}
                    img={e.item_image}
                    price={(e.item_total_price + e.item_deposit).toFixed(2)}
                    onRemove={this.handleRemoveDialog(e)}
                  />
                );
              })}
            </div>
          ) : (
            <div className={classes.cartListEmptyBox}>
              <Typography className={classes.cartListEmptyText}>
                Your cart looks a little bit light
              </Typography>
              <Typography
                onClick={this.handlePage("/")}
                className={classes.continueShopButton}
              >
                Keep shopping
              </Typography>
            </div>
          )}
        </div>

        {!isEmpty(cart[0]) ? (
          <div className={classes.breakdownBox}>
            <PriceList
              title="Total"
              price={!isEmpty(cart) ? cart[0].total.toFixed(2) : 0}
            />
            <Button
              variant="contained"
              color="primary"
              className={classes.checkoutButton}
              onClick={this.handlePage("/checkout", { cart: cart[0] })}
            >
              Checkout
            </Button>
            <Typography
              onClick={this.handlePage("/")}
              className={classes.continueText}
            >
              Continue shopping
            </Typography>
          </div>
        ) : null}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  item: state.item,
});

// export default connect(mapStateToProps, {
//   deleteCartItem,
//   customNotification,
// })(
//   withRouter(
//     withStyles(styles)(withMobileDialog({ breakpoint: "xs" })(CartDrawer))
//   )
// );

export default connect(mapStateToProps, {
  customNotification,
})(
  withStyles(styles)(
    withMobileDialog({ breakpoint: "xs" })(withRouter(CartDrawer))
  )
);
