import React, { Component } from "react";
import { connect } from "react-redux";
import "react-day-picker/lib/style.css";
import DayPicker from "react-day-picker";
import { withStyles } from "@material-ui/core/styles";
import isEmpty from "../../utils/isEmpty";
import {
  Button,
  Collapse,
  Dialog,
  DialogActions,
  DialogContent,
  MenuItem,
  TextField,
  Typography,
} from "@material-ui/core";
import DateConverter from "../../utils/DateConverter";
import TimeConverter from "../../utils/TimeConverter";
import TextInput from "./TextInput";

const styles = (theme) => ({
  root: {
    boxSizing: "border-box",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%",
    margin: "0.7rem 0",
    cursor: "pointer",
  },
  dateShow: {
    fontSize: "1.2rem",
    marginBottom: "1.5rem",
  },
});

class DateTimeBookingField extends Component {
  state = {
    minDate: "",
    maxDate: "",
    mainDialog: false,
    dateContent: false,
    timeContent: false,
    date: null,
    prevDate: null,
    dateToShow: "",
    prevDateToShow: "",
    time: "",
    prevTime: "",
    timeToShow: "",
    prevTimeToShow: "",
    timeList: this.props.timeList,
    dateTime: "",
  };

  onChange = (data) => {
    if (this.props.onChange) {
      this.props.onChange(data);
    }
  };

  componentDidMount() {
    if (!isEmpty(this.state.timeList)) {
      if (!isEmpty(this.props.time) && !isEmpty(this.props.date)) {
        let { date, time } = this.props;
        let d = DateConverter(date.toISOString().split("T")[0]);
        let t = TimeConverter(time);

        this.setState(
          {
            date: date,
            time: time,
            timeToShow: t,
            dateToShow: d,
            prevDate: date,
            prevTime: time,
            prevTimeToShow: t,
            prevDateToShow: d,
          },
          () => {
            this.setState({
              dateTime: `${this.state.dateToShow}, ${this.state.timeToShow}`,
            });
          }
        );
      }
    } else {
      if (!isEmpty(this.props.date)) {
        let { date } = this.props;
        let d = DateConverter(date.toISOString().split("T")[0]);

        this.setState(
          {
            date: date,
            dateToShow: d,
            prevDate: date,
            prevDateToShow: d,
          },
          () => {
            this.setState({
              dateTime: `${this.state.dateToShow}`,
            });
          }
        );
      }
    }
  }

  componentWillReceiveProps(nextProps) {
    if (!isEmpty(this.state.timeList)) {
      if (!isEmpty(nextProps.time) && !isEmpty(nextProps.date)) {
        let { date, time } = nextProps;
        let dt = new Date(date);
        let d = DateConverter(dt.toISOString().split("T")[0]);
        let t = TimeConverter(time);
        this.setState(
          {
            date: dt,
            time: time,
            timeToShow: t,
            dateToShow: d,
            prevDate: dt,
            prevTime: time,
            prevTimeToShow: t,
            prevDateToShow: d,
          },
          () => {
            this.setState({
              dateTime: `${this.state.dateToShow}, ${this.state.timeToShow}`,
            });
          }
        );
      } else {
        this.setState({
          date: null,
          time: "",
          timeToShow: "",
          dateToShow: "",
          prevDate: null,
          prevTime: "",
          prevTimeToShow: "",
          prevDateToShow: "",
          dateTime: "",
        });
      }
    } else {
      if (!isEmpty(nextProps.date)) {
        let { date } = nextProps;
        let dt = new Date(date);
        let d = DateConverter(dt.toISOString().split("T")[0]);

        this.setState(
          {
            date: dt,

            dateToShow: d,
            prevDate: dt,

            prevDateToShow: d,
          },
          () => {
            this.setState({
              dateTime: `${this.state.dateToShow}`,
            });
          }
        );
      } else {
        this.setState({
          date: null,
          time: "",
          timeToShow: "",
          dateToShow: "",
          prevDate: null,
          prevTime: "",
          prevTimeToShow: "",
          prevDateToShow: "",
          dateTime: "",
        });
      }
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.mainDialog && !this.state.mainDialog) {
      if (!isEmpty(this.state.timeList)) {
        let { dateToShow, date, time } = this.state;
        let t = "";
        if (!isEmpty(time)) {
          t = TimeConverter(time);
        }

        this.setState(
          {
            timeToShow: !isEmpty(time) ? t : "",
          },
          () => {
            if (date !== null && !isEmpty(time)) {
              this.setState({
                dateTime: `${dateToShow}, ${this.state.timeToShow}`,
              });

              this.onChange({ date, time });
            } else if (
              this.state.prevDate !== null &&
              !isEmpty(this.state.prevTime)
            ) {
              this.setState(
                {
                  date: this.state.prevDate,
                  dateToShow: this.state.prevDateToShow,
                  time: this.state.prevTime,
                  timeToShow: this.state.prevTimeToShow,
                  dateTime: `${this.state.prevDateToShow}, ${this.state.prevTimeToShow}`,
                },
                () => {
                  this.onChange({
                    date: this.state.date,
                    time: this.state.time,
                  });
                }
              );
            } else {
              this.setState(
                {
                  date: null,
                  dateToShow: "",
                  time: "",
                  timeToShow: "",
                  dateTime: "",
                },
                () => {
                  this.onChange({ date, time });
                }
              );
            }
          }
        );
      } else {
        let d = "";
        if (!isEmpty(this.state.date)) {
          d = DateConverter(this.state.date.toISOString().split("T")[0]);
        }
        this.setState(
          {
            dateToShow: d,
          },
          () => {
            if (this.state.date !== null) {
              this.setState({
                dateTime: `${this.state.dateToShow}`,
              });

              this.onChange({ date: this.state.date, time: this.state.time });
            } else if (this.state.prevDate !== null) {
              this.setState(
                {
                  date: this.state.prevDate,
                  dateToShow: this.state.prevDateToShow,
                },
                () => {
                  this.onChange({
                    date: this.state.date,
                  });
                }
              );
            } else {
              this.setState(
                {
                  date: null,
                  dateToShow: "",

                  dateTime: "",
                },
                () => {
                  this.onChange({
                    date: this.state.date,
                    time: this.state.time,
                  });
                }
              );
            }
          }
        );
      }
    }
    if (
      !prevState.timeContent &&
      this.state.timeContent &&
      this.state.date !== null
    ) {
      this.setState({
        dateToShow: DateConverter(this.state.date.toISOString().split("T")[0]),
      });
    }
    if (isEmpty(prevProps.timeList) && !isEmpty(this.props.timeList)) {
      this.setState({
        timeList: this.props.timeList,
      });
    }
  }

  handleSelect = (name) => (e) => {
    this.setState({
      [name]: e.target.value,
    });
  };

  handleDayClick = (day, { selected, disabled }) => {
    if (disabled) {
      return;
    } else {
      this.setState({
        date: selected ? null : day,
      });
    }
  };

  handleMainDialog = (state) => () => {
    this.setState({
      mainDialog: state,
      dateContent: state,
      timeContent: !state,
    });
  };

  handleDateContent = (state) => {
    this.setState({
      dateContent: state,
    });
  };

  handleTimeContent = (state) => {
    this.setState({
      timeContent: state,
    });
  };

  handleOKMainDialog = () => {
    let { dateContent, timeContent, timeList } = this.state;
    if (!isEmpty(timeList)) {
      if (dateContent) {
        this.handleDateContent(false);
        this.handleTimeContent(true);
      }
      if (timeContent) {
        this.handleMainDialog(false)();
      }
    } else {
      this.handleMainDialog(false)();
    }
  };

  handleCancelMainDialog = () => {
    this.handleMainDialog(false)();
    this.setState({
      date: null,
      dateToShow: "",
      time: "",
      timeToShow: "",
      dateTime: "",
    });
  };

  render() {
    const { classes, className, label, minDate, maxDate, disabled, isTime } =
      this.props;
    const {
      mainDialog,
      dateContent,
      timeContent,
      date,
      dateToShow,
      time,
      timeToShow,
      timeList,
      dateTime,
    } = this.state;
    const disableOK =
      (dateContent && date === null) || (timeContent && isEmpty(time));

    return (
      <div className={className ? className : classes.root}>
        <Dialog open={mainDialog}>
          <DialogContent>
            <Collapse in={dateContent}>
              <DayPicker
                month={minDate ? new Date(minDate) : null}
                selectedDays={date}
                onDayClick={this.handleDayClick}
                disabledDays={[
                  {
                    after: maxDate,
                    before: minDate,
                  },
                ]}
                modifiersStyles={{ selected: { backgroundColor: "#383838" } }}
              />
            </Collapse>
            <Collapse in={timeContent}>
              {!isEmpty(dateToShow) ? (
                <Typography className={classes.dateShow}>
                  {dateToShow}
                </Typography>
              ) : null}
            </Collapse>
            {!isEmpty(timeList) ? (
              <Collapse in={timeContent}>
                <TextInput
                  select
                  variant="outlined"
                  label="Time"
                  style={{ width: "100%" }}
                  value={time}
                  onChange={this.handleSelect("time")}
                >
                  <MenuItem value="" disabled>
                    Select time
                  </MenuItem>
                  {timeList.map((e, i) => (
                    <MenuItem key={i} value={e.value}>
                      {e.label}
                    </MenuItem>
                  ))}
                </TextInput>
              </Collapse>
            ) : null}
          </DialogContent>
          <DialogActions style={{ justifyContent: "space-between" }}>
            <Button onClick={this.handleCancelMainDialog}>Cancel</Button>
            <Button
              variant="contained"
              color="primary"
              disabled={disableOK}
              onClick={this.handleOKMainDialog}
            >
              {dateContent ? "Next" : "Done"}
            </Button>
          </DialogActions>
        </Dialog>

        <TextInput
          disabled={disabled}
          variant="outlined"
          style={{ width: "100%" }}
          label={label}
          onClick={!disabled ? this.handleMainDialog(true) : null}
          value={!isEmpty(dateTime) ? dateTime : label}
          InputProps={{
            readOnly: true,
          }}
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  business: state.business,
  error: state.error,
});

export default connect(mapStateToProps)(
  withStyles(styles)(DateTimeBookingField)
);
