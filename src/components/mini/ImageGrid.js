import React, { Component } from "react";
import isEmpty from "../../utils/isEmpty";

import { withStyles } from "@material-ui/core/styles";
import { isMobile } from "react-device-detect";

const styles = (theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
    boxSizing: "border-box",
    padding: "0 2.5rem 1rem",
    maxWidth: "1300px",
    //justifyContent: "center",
    //marginBottom: "3rem",
    [theme.breakpoints.down("sm")]: {
      padding: "0 0 1rem",
    },
  },
  image: {
    width: "100%",
  },
  mainImgContainer: {
    width: "100%",
  },
  imgListContainer: {
    margin: "1rem 0 0",
    display: "flex",
    flexDirection: "row",
    flexWrap: "wrap",
    width: "100%",
    justifyContent: "center",
  },
  altImgContainer: {
    width: "60px",
    height: "60px",
    borderRadius: "30px",
    boxSizing: "border-box",
    overflow: "hidden",
    margin: "0.5rem",
    cursor: "pointer",
    border: "1px solid gainsboro",
    "&:hover": {
      borderWidth: "2px",
    },
    [theme.breakpoints.down("sm")]: {
      width: "40px",
      height: "40px",
      borderRadius: "20px",
    },
  },
});

class ImageGrid extends Component {
  state = {
    mainImg: this.props.images[0].item_image,
    tempImg: "",
    selected: 1,
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.images !== this.props.images) {
      this.setState({
        mainImg: this.props.images[0].item_image,
        selected: 1,
      });
    }
  }

  handleMainImg = (img, i) => () => {
    this.setState({
      mainImg: img,
      selected: i,
    });
  };

  handleImgHover =
    (data = "") =>
    () => {
      if (!isMobile) {
        this.setState({
          tempImg: data,
        });
      }
    };

  render() {
    const { classes, images, handleClickImage } = this.props;
    const { mainImg, tempImg, selected } = this.state;

    const AltImage = (props) => (
      <div
        onClick={props.onClick}
        onMouseEnter={this.handleImgHover(props.src)}
        onMouseLeave={this.handleImgHover()}
        className={classes.altImgContainer}
        style={
          !props.active ? {} : { border: "2px solid black", padding: "0.1rem" }
        }
      >
        <img src={props.src} alt={props.alt} className={classes.image} />
      </div>
    );

    if (!isEmpty(images)) {
      return (
        <div className={classes.root}>
          <div className={classes.mainImgContainer} onClick={handleClickImage}>
            <img
              src={!isEmpty(tempImg) ? tempImg : mainImg}
              alt={!isEmpty(tempImg) ? tempImg : mainImg}
              className={classes.image}
            />
          </div>
          {images.length > 1 ? (
            <div className={classes.imgListContainer}>
              {images.map((e, i) => (
                <AltImage
                  key={i}
                  src={e.item_image}
                  alt={e.item_image}
                  active={selected === i + 1}
                  onClick={this.handleMainImg(e.item_image, i + 1)}
                />
              ))}
            </div>
          ) : null}
        </div>
      );
    } else {
      return null;
    }
  }
}

export default withStyles(styles)(ImageGrid);
