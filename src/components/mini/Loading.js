import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";

import Fade from "@material-ui/core/Fade";

const styles = (theme) => ({
  paper: {
    backgroundColor: "rgba(0,0,0,0)",
    boxShadow: "none",
  },
});

class Loading extends Component {
  render() {
    const { open } = this.props;
    return (
      <Fade in={open}>
        <div className="lds-css ng-scope">
          <div style={{ width: "100%", height: "100%" }} className="lds-flickr">
            <div />
            <div />
            <div />
          </div>
        </div>
        {/* <div
          className="lds-css ng-scope"
          style={{ width: "200px", height: "200px" }}
        >
          <div
            style={{ width: "100%", height: "100%" }}
            className="lds-eclipse"
          >
            <div />
          </div>
        </div> */}
      </Fade>
    );
  }
}

const mapStateToProps = (state) => ({
  error: state.error,
});

export default connect(mapStateToProps)(withStyles(styles)(Loading));
