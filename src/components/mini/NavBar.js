import React, { Component } from "react";
import { withRouter } from "next/router";
import { Waypoint } from "react-waypoint";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { Typography, Slide, Paper, Button, withWidth, Drawer } from "@material-ui/core";

import CloseIcon from "@material-ui/icons/CloseRounded";
import isEmpty from "../../utils/isEmpty";
import TimeConverter from "../../utils/TimeConverter";
import CartDrawer from "./CartDrawer";

import { mainBgColor, greydark } from "../../utils/ColorPicker";
import { isSafari } from "react-device-detect";
import { nvhDefault, nvhMobile } from "../../utils/navbarHeight";

const styles = theme => ({
	root: {
		display: "flex",
		flexDirection: "column",
		position: "relative",
		width: "100%",
	},
	waypointTriggerBox: {
		height: "47px",
		position: "absolute",
		zIndex: -1,
		width: "100%",
	},

	navBar: {
		width: "100%",
		position: "fixed",
		top: 0,
		zIndex: 1000,
		transition: "all 0.3s",
		display: "flex",
		flexDirection: "column",
		backgroundColor: mainBgColor,
	},
	clearContainer: {
		height: nvhDefault,
		zIndex: -1,
		width: "100%",
		[theme.breakpoints.down("xs")]: {
			height: nvhMobile,
		},
	},
	content: {
		height: nvhDefault,
		zIndex: 100,
		display: "flex",
		flexDirection: "row",
		// justifyContent: "space-between",
		justifyContent: "center",
		alignItems: "center",
		alignSelf: "center",
		width: "100%",
		boxSizing: "border-box",
		padding: "0 1.5rem",
		backgroundColor: "transparent",
		[theme.breakpoints.down("xs")]: {
			height: nvhMobile,
			padding: "0 1rem",
		},
	},

	businessHeader: {
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		justifyContent: "center",
		height: "100%",
		cursor: "pointer",
		[theme.breakpoints.down("xs")]: {
			flexDirection: "column",
			alignItems: "center",
			justifyContent: "center",
		},
	},
	businessLogo: {
		height: "85%",
		borderRadius: "7px",
		overflow: "hidden",
	},
	businessText: {
		fontSize: "1rem",
		marginTop: "0.1rem",
	},
	navContainer: {
		display: "flex",
		flexDirection: "row",
		alignItems: "center",
	},
	navMenu: {
		display: "flex",
		flexDirection: "row",
		justifyContent: "flex-end",
		alignItems: "center",
		padding: "0.7rem 0",
	},
	navButton: {
		display: "flex",
		flexDirection: "row",
		justifyContent: "center",
		alignItems: "center",
		marginLeft: "2rem",
		borderRadius: 10,
		transition: "all 0.3s",
		letterSpacing: "0.5px",
		fontSize: "1rem",
		color: "black",
		"&:hover": {
			backgroundColor: "gainsboro",
			cursor: "pointer",
		},
	},
	navButtonInv: {
		display: "flex",
		flexDirection: "row",
		justifyContent: "center",
		alignItems: "center",
		marginLeft: "2rem",
		borderRadius: 10,
		transition: "all 0.3s",
		letterSpacing: "0.5px",
		fontSize: "1rem",
		color: "white",
		"&:hover": {
			backgroundColor: "dimgrey",
			cursor: "pointer",
		},
	},
	langIcon: {
		fontSize: "1.5rem",
	},
	menuDrawer: {
		height: "100vh",
		backgroundColor: "rgba(0,0,0,1)",
		display: "flex",
		flexDirection: "column",
		padding: "1rem",
		boxSizing: "border-box",
	},
	closeIcon: {
		alignSelf: "flex-end",

		padding: "1rem",
		paddingRight: "0.3rem",
		fontSize: "2.5rem",
		marginBottom: "1rem",
		cursor: "pointer",
	},
	navList: {
		display: "flex",
		flexDirection: "column",
	},
	navText: {
		color: "white",
		fontSize: "2.5rem",
		letterSpacing: "1px",
		fontWeight: "bold",
		padding: "0.7rem 0.5rem",
		cursor: "pointer",
	},
	cartDrawer: {
		height: "100vh",
		width: "80%",
		maxWidth: "900px",
		backgroundColor: "white",
		display: "flex",
		flexDirection: "column",
		//padding: "1rem 0 0",

		boxSizing: "border-box",
		[theme.breakpoints.down("xs")]: {
			width: "100%",
		},
	},
	bizDrawer: {
		height: "100vh",
		width: "80%",
		maxWidth: "600px",
		backgroundColor: "white",
		display: "flex",
		flexDirection: "column",
		justifyContent: "space-between",
		padding: "1rem 0 0",
		boxSizing: "border-box",
		[theme.breakpoints.down("xs")]: {
			width: "100%",
		},
	},
	businessDrawer: {
		width: "100%",
		height: "100%",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		justifyContent: "space-between",
		marginBottom: 20,
		padding: "0 3rem",
		boxSizing: "border-box",
		[theme.breakpoints.down("xs")]: {
			height: isSafari ? "90%" : "100%",
			padding: "0 1rem",
		},
	},
	businessImgContainer: {
		boxShadow: "2px 5px 8px darkgrey",
		width: "125px",
		height: "125px",
		borderRadius: "50%",
		margin: "1rem",
		[theme.breakpoints.down("xs")]: {
			alignSelf: "center",
			width: "75px",
			height: "75px",
		},
	},
	businessImg: {
		maxWidth: "100%",
		maxHeight: "100%",
		borderRadius: "50%",
	},
	businessTitle: {
		fontSize: "2rem",
		fontWeight: 700,
		textAlign: "center",
		[theme.breakpoints.down("xs")]: {
			fontSize: "1.4rem",
		},
	},
	businessDrawerHeader: {
		width: "100%",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
	},
	storeDetails: {
		margin: "1rem 0",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		cursor: "pointer",
		width: "100%",
	},
	contactDetails: {
		margin: "2rem 0 1rem",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		cursor: "pointer",
		width: "100%",
	},
	cityHoursBox: {
		display: "flex",
		flexDirection: "row",
		justifyContent: "flex-start",
		alignItems: "center",

		[theme.breakpoints.down("xs")]: {
			flexDirection: "column",
			alignItems: "flex-start",
		},
	},
	businessOperatingHours: {
		fontSize: "1.2rem",
		textTransform: "uppercase",
		color: greydark,
		fontWeight: 500,
		marginBottom: "0.3rem",
		[theme.breakpoints.down("xs")]: {
			fontSize: "1rem",
		},
	},
	businessAddress: {
		fontSize: "1.3rem",
		textTransform: "uppercase",
		color: greydark,
		fontWeight: 400,
		textAlign: "center",
		[theme.breakpoints.down("xs")]: {
			fontSize: "1.1rem",
		},
	},

	businessPhone: {
		fontSize: "1.3rem",
		color: "dimgrey",
		textTransform: "uppercase",
		letterSpacing: "0.5px",
		fontWeight: 300,
		[theme.breakpoints.down("xs")]: {
			fontSize: "1.1rem",
		},
	},
	businessEmail: {
		fontSize: "1.3rem",
		color: "dimgrey",
		letterSpacing: "0.5px",
		fontStyle: "italic",
		fontWeight: 300,
		[theme.breakpoints.down("xs")]: {
			fontSize: "1.1rem",
		},
	},

	lDescText: {
		fontWeight: 300,
		fontSize: "1.2rem",
		letterSpacing: "0.3px",
	},
});

class NavBar extends Component {
	state = {
		show: false,
		cartDrawer: false,
		bizDrawer: false,
		menu: false,
		langModal: null,
	};

	componentDidUpdate(prevProps, prevState) {
		if (prevProps.notification.redirect !== this.props.notification.redirect) {
			if (this.props.notification.redirect === "AddCart" && this.props.isShowCart) {
				this.handleCartDrawer(true)();
			}
		}
		if (prevState.cartDrawer !== this.state.cartDrawer) {
			this.props.onChangeCart(this.state.cartDrawer);
		}
	}

	handleNavBar = ({ previousPosition, currentPosition, event }) => {
		this.setState({
			show: currentPosition !== Waypoint.inside,
		});
	};

	handlePage = link => () => {
		this.setState({
			menu: false,
			langModal: null,
		});
		if (link.includes("http")) {
			window.open(link);
		} else {
			window.location.href = link;
		}
	};

	handleMenu = state => () => {
		this.setState({
			menu: state,
		});
	};
	handleCartDrawer = state => () => {
		this.setState({
			cartDrawer: state,
		});
	};

	handleBizDrawer = state => () => {
		this.setState({
			bizDrawer: state,
		});
	};
	handleLangModal = state => e => {
		this.setState({
			langModal: state ? e.currentTarget : null,
		});
	};

	render() {
		const { menu, show, cartDrawer, bizDrawer } = this.state;
		const { classes, width, inverted, navs, noFixed, isShowCart, isShowMenu } = this.props;
		const { detail, isFetched } = this.props.business;

		const NavButton = props => {
			return this.props.router.asPath !== props.link ? (
				<Button
					disableRipple
					size="large"
					color="primary"
					color={props.last ? "secondary" : "primary"}
					variant={props.last ? "contained" : "text"}
					style={{
						margin: "0.5rem 0",
						marginLeft: props.last ? "0.5rem" : 0,
						height: "50px",
						padding: !props.last ? "0 1.5rem" : "0 2.5rem",
						fontFamily: "Inter, Arial",
						textTransform: "none",
						fontSize: "1.1rem",
						fontWeight: 500,
						letterSpacing: "0.5px",
						textTransform: "capitalize",
						borderRadius: "5px",
					}}
					onClick={props.action}
				>
					{props.icon ? props.icon : null}
					{props.name}
				</Button>
			) : null;
		};

		const BizDrawer = props => {
			return (
				<div className={classes.businessDrawer}>
					<div className={classes.businessDrawerHeader}>
						<CloseIcon className={classes.closeIcon} onClick={this.handleBizDrawer(false)} />
						<div className={classes.businessImgContainer}>
							<img
								className={classes.businessImg}
								src={!isEmpty(detail.logo) ? detail.logo : "/images/noLogo.png"}
							/>
						</div>
						<Typography className={classes.businessTitle}>{detail.name}</Typography>
						<div className={classes.storeDetails}>
							{!isEmpty(detail.address) ? (
								<Typography className={classes.businessAddress}>{detail.address}</Typography>
							) : null}
							{!isEmpty(detail.open_hour) && !isEmpty(detail.close_hour) ? (
								<div className={classes.cityHoursBox}>
									{!isEmpty(detail.open_hour && detail.close_hour) && (
										<Typography className={classes.businessOperatingHours}>
											{TimeConverter(detail.open_hour)} to {TimeConverter(detail.close_hour)}
										</Typography>
									)}
								</div>
							) : null}
						</div>
					</div>

					<div className={classes.contactDetails}>
						{detail.email && !isEmpty(detail.email) ? (
							<Typography className={classes.businessEmail}>{detail.email}</Typography>
						) : null}
						{detail.phone && !isEmpty(detail.phone) ? (
							<Typography className={classes.businessPhone}>+60{detail.phone}</Typography>
						) : null}
					</div>
					{/* {!isEmpty(detail.description) ? (
            <Typography className={classes.lDescText}>
              {detail.description}
            </Typography>
          ) : (
            <Typography
              className={classes.lDescText}
              style={{ color: "dimgrey" }}
            >
              No description
            </Typography>
          )} */}
				</div>
			);
		};
		const Content = props => (
			<div className={classes.content} style={{ maxWidth: isShowMenu ? "500px" : "none" }}>
				{/* {isShowMenu ? (
          <MenuIcon
            style={{ cursor: "pointer", fontSize: "2rem" }}
            onClick={this.handleBizDrawer(true)}
          />
        ) : null} */}
				{isFetched ? (
					<div className={classes.businessHeader} onClick={this.handlePage(`/`)}>
						<div className={classes.businessLogo}>
							<img
								style={{ maxHeight: "100%" }}
								src={detail && detail.logo ? detail.logo : "/images/noLogo.png"}
								alt={detail && detail.logo ? detail.logo : "noLogo"}
							/>
						</div>

						{/* <Typography className={classes.businessText}>
              {detail.name}
            </Typography> */}
					</div>
				) : null}

				<div className={classes.navContainer}>
					{!isEmpty(navs) ? (
						<div className={classes.navMenu}>
							{navs.map((e, i) => (
								<NavButton
									key={i}
									last={i + 1 === navs.length}
									name={e.title}
									action={this.handlePage(e.action_link)}
									icon={e.icon ? e.icon : null}
								/>
							))}
						</div>
					) : null}

					{/* {isShowCart ? (
            <div
              onClick={this.handleCartDrawer(true)}
              style={{
                position: "relative",
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              {!isEmpty(cart) ? (
                <div
                  style={{
                    position: "absolute",
                    backgroundColor: "#29a9c6",
                    width: "6px",
                    height: "6px",
                    borderRadius: "4px",
                    border: "2px solid white",
                    right: -1,
                    top: -2,
                  }}
                />
              ) : null}
              <CartIcon style={{ fontSize: "1.8rem", cursor: "pointer" }} />
            </div>
          ) : (
            <div />
          )} */}
				</div>
			</div>
		);

		return (
			<div className={classes.root}>
				{noFixed ? (
					<Slide direction="down" in={show} mountOnEnter unmountOnExit>
						<Paper square className={classes.navBar}>
							<Content />
						</Paper>
					</Slide>
				) : null}
				<Waypoint onPositionChange={this.handleNavBar}>
					<div className={classes.waypointTriggerBox} />
				</Waypoint>
				{!noFixed ? <div className={classes.clearContainer} /> : null}
				{noFixed ? (
					<Content />
				) : (
					<Paper square className={classes.navBar} elevation={show ? 3 : 0}>
						<Content />
					</Paper>
				)}
				{isShowMenu ? (
					<Drawer
						anchor="left"
						open={bizDrawer}
						onClose={this.handleBizDrawer(false)}
						classes={{ paper: classes.bizDrawer }}
					>
						<BizDrawer />
					</Drawer>
				) : null}
				{isShowCart ? (
					<Drawer
						anchor="right"
						open={cartDrawer}
						onClose={this.handleCartDrawer(false)}
						classes={{ paper: classes.cartDrawer }}
					>
						<CartDrawer onClose={this.handleCartDrawer(false)} />
					</Drawer>
				) : null}
			</div>
		);
	}
}

const mapStateToProps = state => ({
	business: state.business,
	item: state.item,
	notification: state.notification,
});

export default connect(mapStateToProps)(withStyles(styles)(withWidth()(withRouter(NavBar))));
