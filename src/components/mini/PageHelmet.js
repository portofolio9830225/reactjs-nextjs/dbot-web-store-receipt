import React, { Component } from "react";
import { Helmet } from "react-helmet";

class PageHelmet extends Component {
  render() {
    const { metadata, children } = this.props;

    return (
      <Helmet>
        <title>{metadata.title}</title>
        {metadata.keywords ? (
          <meta name="keywords" content={metadata.keywords} />
        ) : null}

        <meta name="twitter:card" content="summary_large_image" />
        <meta property="og:title" content={metadata.title} key="og:title" />
        <meta name="twitter:title" content={metadata.title} />
        {metadata.image && (
          <meta
            property="og:image"
            content={metadata.image}
            key="og:image"
          ></meta>
        )}
        {metadata.image && (
          <meta name="twitter:image" content={metadata.image}></meta>
        )}

        {metadata.description ? (
          <meta name="description" content={metadata.description} />
        ) : null}
        {metadata.description ? (
          <meta
            property="og:description"
            content={metadata.description}
            key="og:desc"
          />
        ) : null}
        {metadata.description ? (
          <meta name="twitter:description" content={metadata.description} />
        ) : null}

        {metadata.link
          ? metadata.link.map((e, i) => (
              <link key={i} rel={e.rel} hrefLang={e.hreflang} href={e.href} />
            ))
          : null}
        {children}
      </Helmet>
    );
  }
}

export default PageHelmet;
