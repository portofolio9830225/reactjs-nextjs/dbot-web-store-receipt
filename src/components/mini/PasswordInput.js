import React from "react";

import { IconButton, TextField, InputAdornment } from "@material-ui/core";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import TextInput from "./TextInput";

class PasswordInput extends React.Component {
  state = {
    showPass: false,
  };

  handleClickShowPassword = () => {
    this.setState({
      showPass: !this.state.showPass,
    });
  };

  handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  render() {
    const { showPass } = this.state;
    const { theme } = this.props;

    return (
      <TextInput
        {...this.props}
        type={showPass ? "text" : "password"}
        InputProps={{
          endAdornment: (
            <InputAdornment position="end">
              <IconButton
                aria-label="toggle password visibility"
                onClick={this.handleClickShowPassword}
                onMouseDown={this.handleMouseDownPassword}
              >
                {!showPass ? <Visibility /> : <VisibilityOff />}
              </IconButton>
            </InputAdornment>
          ),
        }}
      />
    );
  }
}

export default PasswordInput;
