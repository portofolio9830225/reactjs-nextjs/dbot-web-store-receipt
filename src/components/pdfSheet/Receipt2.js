import React, { Component } from "react";
import { connect } from "react-redux";

import { Typography } from "@material-ui/core";

import DateConverter from "../../utils/DateConverter";
import { secondaryDark } from "../../utils/ColorPicker";
import DurationText from "../../utils/DurationText";
import isEmpty from "../../utils/isEmpty";

class Receipt2Sheet extends Component {
  render() {
    const { bookReceipt } = this.props.booking;

    const IconText = ({ icon, text }) => {
      return (
        <div
          style={{ display: "flex", flexDirection: "row", margin: "0.5rem 0" }}
        >
          <i
            class={icon}
            style={{ marginRight: "1rem", marginTop: "0.15rem" }}
          />
          <Typography
            style={{
              color: "black",
              fontSize: "0.9rem",
              fontWeight: 400,
            }}
          >
            {text}
          </Typography>
        </div>
      );
    };

    const VerticalDivider = (props) => {
      return (
        <div style={{ height: "100%", borderLeft: "1px solid gainsboro" }} />
      );
    };

    const TitleDesc = ({ title, desc }) => {
      return (
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            width: "100%",
          }}
        >
          <Typography style={{ fontSize: "0.8rem" }}>{title}</Typography>
          <Typography style={{ fontWeight: 700, fontSize: "0.9rem" }}>
            {desc}
          </Typography>
        </div>
      );
    };

    const PriceList = ({ title, price }) => {
      return (
        <div
          style={{
            width: "100%",
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "flex-start",
          }}
        >
          <Typography style={{ width: "53%", fontSize: "0.9rem" }}>
            {title}
          </Typography>
          <Typography style={{ fontSize: "0.9rem" }}>:</Typography>
          <Typography
            style={{ width: "45%", fontSize: "0.9rem", textAlign: "right" }}
          >
            RM{price.toFixed(2)}
          </Typography>
        </div>
      );
    };

    const TitleDescRow = ({ title, desc }) => {
      return (
        <div
          style={{
            width: "100%",
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "flex-start",
          }}
        >
          <Typography style={{ width: "53%", fontSize: "0.9rem" }}>
            {title}
          </Typography>
          <Typography style={{ fontSize: "0.9rem" }}>:</Typography>
          <Typography
            style={{ width: "45%", fontSize: "0.9rem", textAlign: "right" }}
          >
            {desc}
          </Typography>
        </div>
      );
    };

    const ItemRow = ({ num, item, rate, deposit }) => {
      return (
        <div
          style={{
            width: "100%",
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
            padding: "1.1rem 0",
            borderTop: "1px solid gainsboro",
          }}
        >
          <Typography style={{ fontWeight: 300, fontSize: "0.9rem" }}>
            {num}
          </Typography>
          <div
            style={{
              width: "90%",
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between",
            }}
          >
            <Typography
              style={{ width: "47%", fontWeight: 700, fontSize: "0.9rem" }}
            >
              {item}
            </Typography>
            <div
              style={{ width: "55%", display: "flex", flexDirection: "column" }}
            >
              <PriceList title="Booking rate" price={rate} />
              <PriceList title="Security deposit" price={deposit} />
              <PriceList title="Subtotal" price={rate + deposit} />
            </div>
          </div>
        </div>
      );
    };

    const TotalRow = ({ total, date, method }) => {
      return (
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "flex-end",
            width: "100%",
            borderTop: "1px solid gainsboro",
            paddingTop: "1rem",
          }}
        >
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              width: "49.5%",
            }}
          >
            <div
              style={{
                width: "100%",
                display: "flex",
                flexDirection: "row",
                justifyContent: "space-between",
                marginBottom: "0.5rem",
                color: secondaryDark,
              }}
            >
              <Typography style={{ fontSize: "1.5rem", fontWeight: 700 }}>
                Total
              </Typography>
              <Typography style={{ fontSize: "1.5rem", fontWeight: 700 }}>
                RM{total.toFixed(2)}
              </Typography>
            </div>
            <TitleDescRow title="Payment date" desc={date} />
            <TitleDescRow title="Payment method" desc={method} />
            <Typography style={{ fontSize: "0.6rem", marginTop: "0.3rem" }}>
              This is a computer generated receipt and no signature is required
            </Typography>
          </div>
        </div>
      );
    };

    const Footer = (props) => {
      return (
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            boxSizing: "border-box",
            width: "100%",
            height: "80px",
            backgroundColor: "#252525",
            padding: "0 2rem",
            position: "absolute",
            bottom: 0,
          }}
        >
          <div
            style={{ display: "flex", flexDirection: "column", width: "17%" }}
          >
            <Typography
              style={{
                fontSize: "0.8rem",
                color: "white",
                marginBottom: "0.5rem",
              }}
            >
              Powered by
            </Typography>
            {/* <img
              src="/images/PinjamTextInverted.png"
              alt="PinjamLogo"
              style={{ width: "100%" }}
            /> */}
          </div>
          <div
            style={{
              width: "73%",
              display: "flex",
              flexDirection: "column",
            }}
          >
            <Typography
              style={{
                color: "white",
                fontSize: "11px",
                width: "100%",
                textTransform: "uppercase",
              }}
            >
              inonity sdn. bhd. (1356436-V)
            </Typography>
            <Typography
              style={{
                color: "white",
                fontSize: "9px",
                width: "100%",
                fontWeight: 300,
              }}
            >
              Menara Solstice, Persiaran Bestari, Cyber 11, 63000 Cyberjaya,
              Selangor Darul Ehsan.
            </Typography>
            <div
              style={{
                display: "flex",
                flexDirection: "row",
                marginTop: "0.5rem",
              }}
            >
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                }}
              >
                <i
                  class="fas fa-globe"
                  style={{
                    fontSize: "0.8rem",
                    color: "white",
                    marginRight: "0.3rem",
                    marginTop: "0.2rem",
                  }}
                ></i>
                <Typography
                  style={{
                    color: "white",
                    fontSize: "0.8rem",
                    fontWeight: 300,
                  }}
                >
                  https://storeup.io
                </Typography>
              </div>
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                  marginLeft: "1.5rem",
                }}
              >
                <i
                  class="fas fa-phone-alt"
                  style={{
                    fontSize: "0.8rem",
                    color: "white",
                    marginRight: "0.3rem",
                    marginTop: "0.2rem",
                  }}
                ></i>
                <Typography
                  style={{
                    color: "white",
                    fontSize: "0.8rem",
                    fontWeight: 300,
                  }}
                >
                  +60 3 8683 1296
                </Typography>
              </div>
            </div>
          </div>
        </div>
      );
    };

    if (this.props.page === 0) {
      return (
        <div
          onLoad={!isEmpty(this.props.onLoad) ? this.props.onLoad : null}
          style={{
            width: 595,
            height: 842,
            overflow: "hidden",
            backgroundColor: "#f7f7f7",
          }}
        >
          <div
            style={{
              position: "relative",
              width: "100%",
              height: "100%",
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <div
              id="receipt2"
              style={{
                display: "flex",
                justifyContent: "space-between",
                //alignItems: "center",
                boxSizing: "border-box",
                width: "100%",
                height: "120px",
                padding: "1rem 2rem 0",
                position: "relative",
              }}
            >
              <div style={{ width: "30%", marginTop: "1.2rem" }}>
                {/* <img
                  src="/images/PinjamText.png"
                  alt="PinjamLogo"
                  style={{ width: "100%" }}
                /> */}
              </div>
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "flex-end",
                  width: "45%",
                }}
              >
                <Typography
                  style={{
                    fontFamily: "Multicolore",
                    fontSize: "3rem",
                    fontWeight: 400,
                    letterSpacing: "0.5rem",
                  }}
                >
                  RECEIPT
                </Typography>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    justifyContent: "space-between",
                    alignItems: "center",
                    width: "100%",
                    //height: "45px",
                    borderTop: "1px solid gainsboro",
                    borderBottom: "1px solid gainsboro",
                    padding: "0.5rem 0.7rem",
                    boxSizing: "border-box",
                  }}
                >
                  <div style={{ width: "48%" }}>
                    <TitleDesc
                      title="Booking ID"
                      desc={bookReceipt.booking_ref}
                    />
                  </div>

                  <VerticalDivider />
                  <div style={{ width: "48%" }}>
                    <TitleDesc
                      title="Receipt date"
                      desc={DateConverter(bookReceipt.receipt_date)}
                    />
                  </div>
                </div>
              </div>
            </div>
            <div
              style={{
                width: "100%",
                boxSizing: "border-box",
                padding: "5px 15px 0",
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
              }}
            >
              <div
                style={{
                  backgroundColor: "white",
                  margin: "10px 0",
                  width: "100%",
                  borderRadius: "25px",
                  padding: "1.5rem",
                  boxSizing: "border-box",
                  //border: "1px solid #ddd",
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "space-between",
                }}
              >
                <div
                  style={{
                    width: "48%",
                    display: "flex",
                    flexDirection: "column",
                  }}
                >
                  <Typography
                    style={{
                      fontFamily: "Multicolore",
                      fontSize: "0.9rem",
                      marginBottom: "1rem",
                    }}
                  >
                    FROM
                  </Typography>
                  <IconText
                    icon="fas fa-user"
                    text={bookReceipt.business_name}
                  />
                  <IconText
                    icon="fas fa-envelope"
                    text={bookReceipt.business_address}
                  />
                  <IconText
                    icon="fas fa-phone-alt"
                    text={`+60${bookReceipt.business_phone}`}
                  />
                </div>
                <div
                  style={{
                    width: "48%",
                    display: "flex",
                    flexDirection: "column",
                  }}
                >
                  <Typography
                    style={{
                      fontFamily: "Multicolore",
                      fontSize: "0.9rem",
                      marginBottom: "1rem",
                    }}
                  >
                    ISSUED TO
                  </Typography>
                  <IconText icon="fas fa-user" text={bookReceipt.renter_name} />
                  <IconText
                    icon="fas fa-envelope"
                    text={bookReceipt.renter_email}
                  />
                  <IconText
                    icon="fas fa-phone-alt"
                    text={`+60${bookReceipt.renter_phone}`}
                  />
                </div>
              </div>

              <div
                style={{
                  backgroundColor: "white",
                  margin: "10px 0",
                  width: "100%",
                  height: "420px",
                  borderRadius: "25px",
                  padding: "1.5rem",
                  boxSizing: "border-box",
                  //border: "1px solid #ddd",
                  display: "flex",
                  flexDirection: "column",
                }}
              >
                <Typography
                  style={{
                    fontFamily: "Multicolore",
                    fontSize: "0.9rem",
                    marginBottom: "1rem",
                  }}
                >
                  DESCRIPTION
                </Typography>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    justifyContent: "space-between",
                    alignItems: "center",
                    width: "100%",
                    //height: "45px",
                    borderTop: "1px solid gainsboro",
                    borderBottom: "1px solid gainsboro",
                    padding: "0.5rem 0.7rem",
                    boxSizing: "border-box",
                  }}
                >
                  <div
                    style={{
                      width: "23%",
                    }}
                  >
                    <TitleDesc
                      title="Booking ref."
                      desc={bookReceipt.booking_ref}
                    />
                  </div>
                  <VerticalDivider />
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      width: "23%",
                    }}
                  >
                    <TitleDesc
                      title="Start date"
                      desc={DateConverter(bookReceipt.start_date)}
                    />
                  </div>
                  <VerticalDivider />
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      width: "23%",
                    }}
                  >
                    <TitleDesc
                      title="End date"
                      desc={DateConverter(bookReceipt.end_date)}
                    />
                  </div>
                  <VerticalDivider />
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      width: "23%",
                    }}
                  >
                    <TitleDesc
                      title="Duration"
                      desc={DurationText(bookReceipt.duration)}
                    />
                  </div>
                </div>
                <div
                  style={{
                    width: "100%",
                    display: "flex",
                    flexDirection: "row",
                    justifyContent: "space-between",
                    margin: "1.5rem 0 0.5rem",
                  }}
                >
                  <Typography style={{ fontSize: "0.8rem" }}>No.</Typography>
                  <div
                    style={{
                      width: "90%",
                      display: "flex",
                      flexDirection: "row",
                      justifyContent: "space-between",
                    }}
                  >
                    <Typography style={{ fontSize: "0.8rem" }}>
                      Item(s)
                    </Typography>
                    <Typography style={{ fontSize: "0.8rem" }}>
                      Amount(RM)
                    </Typography>
                  </div>
                </div>
                {!isEmpty(bookReceipt) &&
                  bookReceipt.items.map((e, i) => {
                    if (i + 1 <= 3) {
                      return (
                        <ItemRow
                          key={i}
                          num={i + 1}
                          item={e.item_title}
                          rate={e.item_price}
                          deposit={e.item_deposit}
                        />
                      );
                    }
                  })}

                {bookReceipt.items.length < 3 && (
                  <TotalRow
                    total={500}
                    date="31 Mar 2021, 07:42"
                    method="Direct debit via FPX"
                  />
                )}
              </div>
            </div>
            <Footer />
          </div>
        </div>
      );
    } else {
      return (
        <div
          onLoad={!isEmpty(this.props.onLoad) ? this.props.onLoad : null}
          style={{
            width: 595,
            height: 842,
            overflow: "hidden",
            backgroundColor: "#f7f7f7",
            //zIndex: -999,
            //position: "fixed",
          }}
        >
          <div
            style={{
              position: "relative",
              width: "100%",
              height: "100%",
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <div
              style={{
                width: "100%",
                boxSizing: "border-box",
                padding: "5px 15px 0",
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
              }}
            >
              <div
                style={{
                  backgroundColor: "white",
                  margin: "10px 0",
                  width: "100%",
                  height: "719px",
                  borderRadius: "25px",
                  padding: "1.5rem",
                  boxSizing: "border-box",
                  //border: "1px solid #ddd",
                  display: "flex",
                  flexDirection: "column",
                }}
              >
                {this.props.booking.bookReceipt.items.length !==
                  3 + 8 * (this.props.page - 1) && (
                  <div
                    style={{
                      width: "100%",
                      display: "flex",
                      flexDirection: "row",
                      justifyContent: "space-between",
                      margin: "0 0 0.5rem",
                    }}
                  >
                    <Typography style={{ fontSize: "0.8rem" }}>No.</Typography>
                    <div
                      style={{
                        width: "90%",
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "space-between",
                      }}
                    >
                      <Typography style={{ fontSize: "0.8rem" }}>
                        Item(s)
                      </Typography>
                      <Typography style={{ fontSize: "0.8rem" }}>
                        Amount(RM)
                      </Typography>
                    </div>
                  </div>
                )}
                {!isEmpty(bookReceipt) &&
                  bookReceipt.items.map((e, i) => {
                    if (
                      i + 1 > 3 + 8 * (this.props.page - 1) &&
                      i + 1 <= 3 + 8 * this.props.page
                    ) {
                      return (
                        <ItemRow
                          key={i}
                          num={i + 1}
                          item={e.item_title}
                          rate={e.item_price}
                          deposit={e.item_deposit}
                        />
                      );
                    }
                  })}

                {this.props.booking.bookReceipt.items.length <
                  3 + 8 * this.props.page && (
                  <TotalRow
                    total={500}
                    date="31 Mar 2021, 07:42"
                    method="Direct debit via FPX"
                  />
                )}
              </div>
            </div>
            <Footer />
          </div>
        </div>
      );
    }
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  booking: state.booking,
  profile: state.profile,
});

export default connect(mapStateToProps)(Receipt2Sheet);
