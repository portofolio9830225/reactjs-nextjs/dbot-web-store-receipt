import React, { Component } from "react";
import html2canvas from "html2canvas";
import { connect } from "react-redux";
import { withRouter } from "next/router";
import { withStyles } from "@material-ui/core/styles";
import { Button, Typography } from "@material-ui/core";
import { getBookDetails } from "../../store/actions/bookingAction";
import { customNotification } from "../../store/actions/notificationAction";
import { setLoading } from "../../store/actions/loadingAction";
import isEmpty from "../../utils/isEmpty";
import { errorColor, grey, greydark, greylight, successColor } from "../../utils/ColorPicker";
import DateTimeGetter from "../../utils/DateTimeGetter";

const styles = theme => ({
	root: {
		display: "flex",
		flexDirection: "column",
		width: "100%",
		maxWidth: "660px",
		padding: "2rem 1rem",
		boxSizing: "border-box",
		margin: "0 auto",
		backgroundColor: "#fff",
		borderRadius: "1rem",
		alignItems: "center",
	},

	header: {
		width: "100%",
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		marginBottom: "2.5rem",
	},

	receiptTitle: {
		fontSize: "3rem",
		fontWeight: 400,
		color: "grey",
	},

	receiptInfo: {
		width: "100%",
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-between",
	},

	receiptInfoLeft: {
		width: "50%",
		display: "flex",
		flexDirection: "column",
	},

	receiptInfoDetail: {
		width: "100%",
		height: "50%",
		border: "0.5px solid white",
		display: "flex",
		flexDirection: "column",
		justifyContent: "center",
		backgroundColor: "rgb(250, 250, 250)",
		padding: "0 1.3rem",
		boxSizing: "border-box",
	},

	receiptInfoDetailTitle: {
		textTransform: "uppercase",
		fontSize: "1rem",
		color: "dimgrey",
	},

	receiptInfoDetailDesc: {
		fontSize: "1.2rem",
		marginTop: "2px",
	},

	receiptInfoRight: {
		width: "50%",
		display: "flex",
		flexDirection: "column",
	},

	productContainer: {
		width: "100%",
		marginTop: "2rem",
	},
	contactButtonHeader: {
		marginTop: "1rem",
		alignSelf: "flex-start",
		height: "auto",
		[theme.breakpoints.down("xs")]: {
			fontSize: "0.8rem",
		},
	},
	productContainerTitle: {
		width: "100%",
		backgroundColor: "rgb(250, 250, 250)",
		padding: "0.5rem 1.3rem",
		fontSize: "1.5rem",
		fontWeight: 700,
		boxSizing: "border-box",
	},

	productBox: {
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-between",
		width: "100%",
		marginTop: "0.8rem",
		padding: "0 1.3rem",
		paddingTop: "0.8rem",
		boxSizing: "border-box",
		borderTop: "1px solid rgb(238, 238, 238)",
	},
	productBoxImgContainer: {
		width: "61px",
		height: "61px",
		position: "relative",
		[theme.breakpoints.down("xs")]: {
			width: "42px",
			height: "42px",
		},
	},
	productBoxImg: {
		maxWidth: "100%",
		maxHeight: "100%",
		borderRadius: "0.5rem",
	},

	productBoxInfo: {
		width: "calc(97% - 61px)",
		display: "flex",
		flexDirection: "column",
		justifyContent: "space-evenly",
		[theme.breakpoints.down("xs")]: {
			width: "calc(97% - 42px)",
		},
	},

	productBoxTitle: {
		fontWeight: 600,
		fontSize: "1.2rem",
		[theme.breakpoints.down("xs")]: {
			fontSize: "1rem",
		},
	},

	productBoxQuantity: {
		fontSize: "1.2rem",
		color: "dimgrey",
		[theme.breakpoints.down("xs")]: {
			fontSize: "1rem",
		},
	},

	productBoxTotal: {
		fontSize: "1.2rem",
		textAlign: "right",
		fontWeight: 600,
		[theme.breakpoints.down("xs")]: {
			fontSize: "1rem",
		},
	},

	productBoxAnotherPrice: {
		fontSize: "1rem",
		textAlign: "right",
		textDecoration: "line-through",
		// fontWeight: 600,
		color: grey,
		[theme.breakpoints.down("xs")]: {
			fontSize: "0.8rem",
		},
	},

	totalContainer: {
		width: "100%",
		display: "flex",
		flexDirection: "column",
		alignItems: "flex-end",
		marginTop: "1.5rem",
	},

	subtotalBox: {
		width: "100%",
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-between",
		marginTop: "0.8rem",
	},

	totalBox: {
		width: "100%",
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		borderTop: "1px solid rgb(238, 238, 238)",
		borderBottom: "1px solid rgb(238, 238, 238)",
		marginTop: "1.5rem",
	},

	subtotalBoxLeft: {
		width: "65%",
		display: "flex",
		flexDirection: "row",
		justifyContent: "flex-end",
	},

	subtotalBoxRight: {
		width: "27%",
		display: "flex",
		flexDirection: "row",
		justifyContent: "flex-end",
		paddingRight: "1.3rem",
		boxSizing: "border-box",
	},

	totalBoxLeft: {
		width: "65%",
		display: "flex",
		flexDirection: "row",
		justifyContent: "flex-end",
	},

	totalBoxRight: {
		width: "30%",
		display: "flex",
		flexDirection: "row",
		justifyContent: "flex-end",
		paddingRight: "1.3rem",
		boxSizing: "border-box",
	},

	horizontalLine: {
		borderRight: "1px solid rgb(238, 238, 238)",
	},

	totalBoxTitle: {
		fontSize: "1.2rem",
		color: "dimgrey",
	},

	totalBoxDesc: {
		fontSize: "1.2rem",
		color: "black",
		fontWeight: 600,
	},
	paymentStatusContainer: {
		display: "flex",
		alignItems: "center",
		justifyContent: "center",
		alignSelf: "flex-end",
		marginTop: "0.5rem",
		padding: "0 1.5rem",
		height: "24px",
		borderRadius: "12px",
		marginRight: "1.3rem",
	},
	paymentStatusText: {
		color: "white",
		fontWeight: 600,
		letterSpacing: "0.2px",
		lineHeight: "12px",
	},
	receiptFooterContainer: {
		width: "100%",
		marginTop: "2rem",
	},

	receiptFooterText: {
		textAlign: "center",
		fontSize: "1.2rem",
	},

	footerContainer: {
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		width: "100%",
		marginTop: "5rem",
	},

	footerText: {
		textAlign: "center",
		fontSize: "1.2rem",
	},
});

class Receipt3 extends Component {
	handleContact = () => {
		let { bookDetails } = this.props.booking;
		let text = `[  _Order ID:_ *${bookDetails.ref}*  ]\n\nHi ${bookDetails.business.name}, ${bookDetails.customer.name} here.\nMay I know the status of my order?`;

		setTimeout(() => {
			window.location.assign(`https://wa.me/60${bookDetails.business.phone}?text=${encodeURI(text)}`);
		}, 1000);
	};

	render() {
		const { classes, toPrint } = this.props;
		const { bookDetails, bookItems, bookPayment, bookShipping, bookPromo } = this.props.booking;
		// const loading = this.props.loading.status;
		// console.log(bookItems);

		return (
			<div className={classes.root}>
				<div
					className={classes.header}
					style={{
						paddingLeft: "1.2rem",
						paddingRight: "1.2rem",
						boxSizing: "border-box",
					}}
				>
					<img
						src="https://ik.imagekit.io/gb7xb1lfuzg/assets/LogoWOText__wqPUUbqqi.png?ik-sdk-version=javascript-1.4.3&updatedAt=1647920577191"
						style={{ width: "40px" }}
					/>

					<Typography className={classes.receiptTitle}>
						{bookPayment && bookPayment.paid ? "Receipt" : "Invoice"}
					</Typography>
				</div>
				<div className={classes.receiptInfo}>
					<div className={classes.receiptInfoLeft}>
						<div className={classes.receiptInfoDetail}>
							<Typography className={classes.receiptInfoDetailTitle}>Billed from</Typography>

							<Typography
								className={classes.receiptInfoDetailDesc}
								style={{
									width: "100%",
									whiteSpace: "nowrap",
									overflow: "hidden",
									textOverflow: "ellipsis",
								}}
							>
								{bookDetails.business.registered_name || bookDetails.business.name}
							</Typography>
						</div>
						<div className={classes.receiptInfoDetail}>
							<Typography className={classes.receiptInfoDetailTitle}>Order ID</Typography>

							<Typography className={classes.receiptInfoDetailDesc}>{bookDetails.ref}</Typography>
						</div>
						<div className={classes.receiptInfoDetail}>
							<Typography className={classes.receiptInfoDetailTitle}>Date</Typography>

							<Typography className={classes.receiptInfoDetailDesc}>
								<DateTimeGetter
									dt={
										process.env.NODE_ENV === "production" && bookPayment.paid
											? bookPayment.paid_at
											: bookDetails.created_at
									}
								/>
							</Typography>
						</div>
					</div>
					<div className={classes.receiptInfoRight}>
						<div className={classes.receiptInfoDetail} style={{ height: "auto", padding: "2.5rem 1.3rem" }}>
							<Typography className={classes.receiptInfoDetailTitle}>Billed to</Typography>

							<Typography
								className={classes.receiptInfoDetailDesc}
								style={{
									width: "100%",
									whiteSpace: "nowrap",
									overflow: "hidden",
									textOverflow: "ellipsis",
								}}
							>
								{bookDetails.customer.name}
							</Typography>

							{!isEmpty(bookShipping) ? (
								<Typography className={classes.receiptInfoDetailDesc}>
									{bookShipping.address_1} {bookShipping.address_2} {bookShipping.address_3}{" "}
									{bookShipping.address_4}, {bookShipping.postcode} {bookShipping.city}{" "}
									{bookShipping.state}, {bookShipping.country}
								</Typography>
							) : (
								<>
									<Typography className={classes.receiptInfoDetailDesc}>
										{bookDetails.customer.email}
									</Typography>
									<Typography className={classes.receiptInfoDetailDesc}>
										+60{bookDetails.customer.phone}
									</Typography>
								</>
							)}
						</div>
					</div>
				</div>
				{/* <Button color="secondary" className={classes.contactButtonHeader} onClick={this.handleContact}>
				 	Contact Seller
				 </Button> */}
				{!toPrint && !isEmpty(bookDetails.business.phone) ? (
					<a
						href={`https://wa.me/60${bookDetails.business.phone}`}
						style={{
							textDecoration: "none",
							marginTop: "1rem",
							alignSelf: "flex-start",
							height: "auto",
							color: "#007afe",
							borderRadius: "15px",
							height: "30px",
							backgroundColor: "rgba(250, 250, 250,1)",
							textTransform: "uppercase",
							verticalAlign: "center",
							textAlign: "center",
							display: "flex",
							justifyContent: "center",
							alignItems: "center",
							padding: "0 1rem",
						}}
						target="_blank"
					>
						Contact seller
					</a>
				) : null}
				{/* <div className={classes.receiptInfoDetail}>
					<Typography className={classes.receiptInfoDetailTitle}>Seller's Whastapp</Typography>

					<Typography className={classes.receiptInfoDetailDesc}>+60{bookDetails.business_phone}</Typography>
				</div> */}
				<div className={classes.productContainer}>
					<Typography className={classes.productContainerTitle}>Product</Typography>

					{bookItems.map((e, i) => {
						return (
							<div key={i} className={classes.productBox} style={{ borderColor: "transparent" }}>
								<div className={classes.productBoxImgContainer}>
									<img className={classes.productBoxImg} src={e.image} />
									{!isEmpty(e.another_price) && (
										<div
											style={{
												position: "absolute",
												width: "44px",
												backgroundColor: "#FF9500",
												padding: "0.2rem 0.03rem",
												bottom: "-3px",
												textAlign: "center",
												fontSize: "0.55rem",
												borderRadius: "12px",
												left: "10px",
												color: "white",
											}}
										>
											{parseInt(((e.price - e.another_price) / e.price) * 100)}% OFF
										</div>
									)}
								</div>
								<div className={classes.productBoxInfo}>
									<Typography className={classes.productBoxTitle}>{e.name}</Typography>
									<Typography className={classes.productBoxQuantity} style={{ fontWeight: 400 }}>
										{e.variant_name}
									</Typography>
									<Typography className={classes.productBoxQuantity}>
										Quantity: {e.quantity}
									</Typography>

									{isEmpty(e.another_price) ? (
										<Typography className={classes.productBoxTotal}>
											RM{(e.price * e.quantity).toFixed(2)}
										</Typography>
									) : (
										<div>
											<Typography className={classes.productBoxTotal}>
												RM{e.another_price.toFixed(2)}
											</Typography>
											<Typography className={classes.productBoxAnotherPrice}>
												RM{(e.price * e.quantity).toFixed(2)}
											</Typography>
										</div>
									)}
								</div>
							</div>
						);
					})}
				</div>
				<div className={classes.totalContainer}>
					<div className={classes.subtotalBox}>
						<div className={classes.subtotalBoxLeft}>
							<Typography className={classes.totalBoxTitle}>Subtotal</Typography>
						</div>
						<div className={classes.subtotalBoxRight}>
							<Typography className={classes.totalBoxDesc}>
								RM{bookDetails.product_subtotal.toFixed(2)}
							</Typography>
						</div>
					</div>
					{!isEmpty(bookShipping) && (
						<div className={classes.subtotalBox}>
							<div className={classes.subtotalBoxLeft}>
								<Typography className={classes.totalBoxTitle}>Shipping</Typography>
							</div>
							<div className={classes.subtotalBoxRight}>
								<Typography className={classes.totalBoxDesc}>
									RM{bookShipping.price.toFixed(2)}
								</Typography>
							</div>
						</div>
					)}
					{!isEmpty(bookPromo) && (
						<div className={classes.subtotalBox}>
							<div className={classes.subtotalBoxLeft}>
								<Typography className={classes.totalBoxTitle}>Promo ({bookPromo.code})</Typography>
							</div>
							<div className={classes.subtotalBoxRight}>
								<Typography className={classes.totalBoxDesc}>
									-RM{bookPromo.amount.toFixed(2)}
								</Typography>
							</div>
						</div>
					)}
					<div className={classes.totalBox}>
						<div className={classes.totalBoxLeft}>
							<Typography
								className={classes.totalBoxTitle}
								style={{ textTransform: "uppercase", fontWeight: 600 }}
							>
								Total
							</Typography>
						</div>
						<div className={classes.horizontalLine} style={{ padding: "1.5rem 0" }}>
							<br />
						</div>
						<div className={classes.totalBoxRight}>
							<Typography className={classes.totalBoxDesc} style={{ fontSize: "1.7rem" }}>
								RM
								{bookDetails.grand_total.toFixed(2)}
							</Typography>
						</div>
					</div>
				</div>
				<div
					className={classes.paymentStatusContainer}
					style={{
						backgroundColor:
							bookDetails.status === "rejected"
								? errorColor
								: bookPayment && bookPayment.paid
								? successColor
								: greylight,
					}}
				>
					<Typography className={classes.paymentStatusText}>
						{bookDetails.status === "rejected"
							? "REJECTED"
							: bookPayment && bookPayment.paid
							? "PAID"
							: "UNPAID"}
					</Typography>
				</div>
				<div
					className={classes.receiptFooterContainer}
					style={{
						boxSizing: "border-box",
						paddingRight: "1.2rem",
						paddingLeft: "1.2rem",
					}}
				>
					<Typography className={classes.receiptFooterText}>
						Get help with purchases at
						<span
							style={{ color: "#0071e3", cursor: "pointer" }}
							onClick={() => {
								window.open("https://storeup.tawk.help/");
							}}
						>
							{" "}
							StoreUp Help Center{" "}
						</span>
						or email StoreUp Team at
						<span
							style={{ color: "#0071e3", cursor: "pointer" }}
							onClick={() => {
								window.open("mailto:support@storeup.io");
							}}
						>
							{" "}
							support@storeup.io
						</span>
					</Typography>
				</div>
				<div className={classes.footerContainer}>
					<img
						src="https://ik.imagekit.io/gb7xb1lfuzg/assets/LogoWText_es8V0pea5nBC.png?ik-sdk-version=javascript-1.4.3&updatedAt=1647920576914"
						style={{ width: "35px" }}
					/>

					<Typography className={classes.footerText} style={{ margin: "1.5rem" }}>
						<span
							style={{ color: "#0071e3", cursor: "pointer" }}
							onClick={() => {
								window.open("https://www.storeup.io/terms");
							}}
						>
							Terms of Service
						</span>
						&nbsp; &bull; &nbsp;
						<span
							style={{ color: "#0071e3", cursor: "pointer" }}
							onClick={() => {
								window.open("https://www.storeup.io/privacy");
							}}
						>
							Privacy Policy
						</span>
					</Typography>

					<Typography className={classes.footerText} style={{ marginBottom: "0.3rem" }}>
						2021 StoreUp. All rights reserved.
					</Typography>

					<Typography className={classes.footerText}>Inonity Sdn. Bhd. (1356436-V)</Typography>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	auth: state.auth,
	business: state.business,
	booking: state.booking,
	item: state.item,
	//   loading: state.loading,
	error: state.error,
});

export default connect(mapStateToProps, {
	getBookDetails,

	setLoading,
	customNotification,
})(withStyles(styles)(withRouter(Receipt3)));
