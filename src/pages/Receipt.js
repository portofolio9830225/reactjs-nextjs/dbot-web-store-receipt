import React, { Component } from "react";
import html2canvas from "html2canvas";
import PageHelmet from "../components/mini/PageHelmet";
import { connect } from "react-redux";
import { withRouter } from "next/router";
import { withStyles } from "@material-ui/core/styles";
import {
	Button,
	Checkbox,
	Collapse,
	Dialog,
	Fade,
	FormControlLabel,
	IconButton,
	InputAdornment,
	TextField,
	Typography,
} from "@material-ui/core";
import { getBankDetails, getBookDetails, submitCustomerEmail } from "../store/actions/bookingAction";
import { customNotification } from "../store/actions/notificationAction";
import { setLoading } from "../store/actions/loadingAction";
import isEmpty from "../utils/isEmpty";

import CheckIcon from "@material-ui/icons/CheckCircleOutlineRounded";
import {
	CloseRounded,
	SystemUpdateAltRounded,
	HighlightOffRounded,
	Brightness1,
	Stop,
	SendRounded,
	ChevronLeftRounded,
} from "@material-ui/icons";
import Receipt from "../components/pdfSheet/Receipt3";
import { black, greydark, greywhite, mainBgColor, secondary } from "../utils/ColorPicker";
import NotFound from "../components/NotFound";
import { isMobileSafari } from "react-device-detect";
import ListBank from "../utils/ListBank";

const styles = theme => ({
	root: {
		boxSizing: "border-box",
		width: "100%",
		height: "100%",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		justifyContent: "center",
		marginBottom: "7rem",
		alignSelf: "center",
		backgroundColor: mainBgColor,
		position: "relative",
		marginTop: "1rem",
		[theme.breakpoints.down("xs")]: {
			overflowX: "hidden",
		},
	},

	bookingCard: {
		width: "100%",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		backgroundColor: "#f7f7f7",
		padding: "2rem 3rem",
		boxSizing: "border-box",
		borderRadius: "10px",
		marginBottom: "3rem",
		[theme.breakpoints.down("xs")]: {
			borderRadius: "6px",
			padding: "2rem 1rem",
			marginBottom: "2rem",
		},
	},

	textField: {
		alignSelf: "center",
		width: "100%",
		margin: "1rem 0 0",
	},
	header: {
		width: "100%",
		display: "flex",
		alignItems: "flex-start",
		[theme.breakpoints.down("xs")]: {
			alignItems: "center",
		},
	},
	headerTitle: {
		fontSize: "1.8rem",
		textTransform: "uppercase",
		fontWeight: 500,
		margin: "1rem 0",
		[theme.breakpoints.down("xs")]: {
			fontSize: "1.5rem",
			margin: "0 0 0.5rem",
		},
	},
	headerItemCounter: {
		color: "dimgrey",
		fontSize: "1.4rem",
		marginBottom: "1rem",
		[theme.breakpoints.down("xs")]: {
			fontSize: "1.2rem",
		},
	},
	headerSeeAll: {
		cursor: "pointer",
		padding: "1rem 1rem 1rem 0",
		color: "dimgrey",
		fontSize: "1.3rem",
		fontWeight: 300,
		[theme.breakpoints.down("xs")]: {
			fontSize: "1.1rem",
			paddingBottom: 0,
		},
	},
	headerPrice: {
		fontSize: "1.4rem",
		fontWeight: 300,
		[theme.breakpoints.down("xs")]: {
			fontSize: "1.3rem",
		},
	},
	headerDuration: {
		fontSize: "1.3rem",
		fontWeight: 300,
		color: "dimgrey",
		fontStyle: "italic",
		[theme.breakpoints.down("xs")]: {
			fontSize: "1.2rem",
		},
	},
	ownerSection: {
		width: "100%",
		display: "flex",
		flexDirection: "row",
		alignItems: "center",
		marginBottom: "2rem",
		[theme.breakpoints.down("xs")]: {
			marginBottom: 0,
		},
	},
	ownerImg: {
		borderRadius: "50%",
		width: "12%",
		maxWidth: "70px",
		[theme.breakpoints.down("xs")]: {
			width: "14%",
		},
	},
	ownerDetail: {
		boxSizing: "border-box",
		padding: "0 1.5rem",
		display: "flex",
		flexDirection: "column",
		justifyContent: "space-between",
		[theme.breakpoints.down("xs")]: {
			padding: "0 1rem",
		},
	},
	ownerName: {
		fontSize: "1.5rem",
		[theme.breakpoints.down("xs")]: {
			fontSize: "1.1rem",
		},
	},
	ownerAddress: {
		fontSize: "1.2rem",
		color: "dimgrey",
		margin: "0.3rem 0",
		[theme.breakpoints.down("xs")]: {
			fontSize: "1rem",
		},
	},

	// verifySection: {
	//   margin: "0.3rem 0"
	// },
	icon: {
		fontSize: "1.7rem",
		marginRight: "0.5rem",
		[theme.breakpoints.down("xs")]: {
			fontSize: "1.2rem",
			marginRight: "0.3rem",
		},
	},
	verified: {
		color: "#1c9c91",
	},
	unverified: {
		color: "darkgrey",
	},
	detailList: {
		margin: "0.5rem 0",
		width: "100%",
		display: "flex",
		flexDirection: "row",
		justifyContent: "center",
	},
	detailTitle: {
		textAlign: "right",
		width: "45%",
		fontSize: "1.4rem",
		color: "#888",
	},
	detailContent: {
		boxSizing: "border-box",
		padding: "0 1rem",
		width: "55%",
		fontSize: "1.4rem",
		color: "#000",
	},
	priceSection: {
		width: "100%",
		display: "flex",
		flexDirection: "column",
	},
	bankSection: {
		width: "100%",
		display: "flex",
		flexDirection: "column",
	},
	bankTitle: {
		width: "100%",
		fontSize: "2.3rem",
		fontWeight: 500,
		margin: "1rem 0",
		alignSelf: "flex-start",
		[theme.breakpoints.down("xs")]: {
			fontSize: "1.8rem",
		},
	},
	bankBox: {
		width: "25%",
		maxWidth: "175px",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		borderRadius: 5,
		margin: "1rem",
	},
	bankImg: {
		width: "100%",
		borderRadius: 5,
	},
	bankName: {
		fontSize: "1.5rem",
		textAlign: "center",
	},
	bankStatus: {
		fontSize: "1.2rem",
	},
	active: {
		color: "seagreen",
	},
	inactive: {
		color: "firebrick",
	},
	notice: {
		letterSpacing: "1px",
		fontSize: "1.7rem",
		color: "dimgrey",
		margin: "1rem 0",
		textAlign: "center",
		[theme.breakpoints.down("xs")]: {
			fontSize: "1.4rem",
		},
	},
	priceList: {
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-between",
		width: "100%",
		margin: "0.6rem 0",
		[theme.breakpoints.down("xs")]: {
			margin: "0.3rem 0",
		},
	},
	priceListTitle: {
		fontSize: "1.4rem",
		color: "dimgrey",
		[theme.breakpoints.down("xs")]: {
			fontSize: "1.2rem",
		},
	},
	priceListDesc: {
		fontSize: "1.4rem",
		fontWeight: 500,
		[theme.breakpoints.down("xs")]: {
			fontSize: "1.2rem",
		},
	},
	priceListTitleBold: {
		fontWeight: "bold",
		textTransform: "uppercase",
		fontSize: "2.1rem",
		color: "dimgrey",
		[theme.breakpoints.down("xs")]: {
			fontSize: "1.5rem",
		},
	},

	priceListDescBold: {
		fontSize: "2.1rem",
		fontWeight: "bold",
		[theme.breakpoints.down("xs")]: {
			fontSize: "1.5rem",
		},
	},
	dateContainer: {
		width: "100%",
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-between",
		marginTop: "1.7rem",
		[theme.breakpoints.down("xs")]: {
			marginTop: "0.5rem",
		},
	},
	dateList: {
		display: "flex",
		flexDirection: "column",
		width: "30%",
	},
	dateListTitle: {
		fontSize: "1.2rem",
		color: "dimgrey",
		marginBottom: "0.1rem",
		[theme.breakpoints.down("xs")]: {
			fontSize: "1rem",
		},
	},
	dateListDesc: {
		fontSize: "1.5rem",
		fontWeight: 500,
		[theme.breakpoints.down("xs")]: {
			fontSize: "1.1rem",
		},
	},
	bookingRef: {
		fontSize: "1.3rem",
		fontStyle: "italic",
		width: "100%",
		fontWeight: 300,
		marginBottom: "1rem",
		[theme.breakpoints.down("xs")]: {
			fontSize: "1.2rem",
		},
	},
	paperScrollPaper: {
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		boxSizing: "border-box",
		margin: "auto",
		padding: "1rem 1.5rem",
		minHeight: "100vh",
		minWidth: "100vw",
		overflowX: "hidden",
	},
	closeIcon: {
		alignSelf: "flex-end",
		color: "dimgrey",
		padding: "2rem",
		fontSize: "2.8rem",
		marginBottom: "1rem",
		cursor: "pointer",
		[theme.breakpoints.down("xs")]: {
			fontSize: "2.3rem",
			padding: "0.5rem",
		},
	},
	itemDialogContent: {
		width: "100%",
		maxWidth: "900px",
		alignSelf: "center",
		display: "flex",
		flexDirection: "column",
		padding: "3rem 0",
		[theme.breakpoints.down("xs")]: {
			padding: "1rem 0",
		},
	},
	itemList: {
		alignSelf: "center",
		width: "100%",
		maxWidth: "900px",
		padding: "1rem 0",
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "flex-start",
		[theme.breakpoints.down("xs")]: {
			alignItems: "center",
		},
	},
	itemListContent: {
		width: "75%",
		display: "flex",
		flexDirection: "column",
	},
	itemListPriceBox: {
		width: "50%",
		display: "flex",
		flexDirection: "column",
		[theme.breakpoints.down("xs")]: {
			minWidth: "175px",
		},
	},
	contentSucess: {
		display: "flex",
		flexDirection: "column",
		width: "90%",
		maxWidth: "600px",
		height: "100%",
		boxSizing: "border-box",
	},
	statusIcon: {
		margin: "2rem auto 0.5rem",
		width: "100%",
		fontSize: "12rem",
		textAlign: "center",
	},
	welcomeText: {
		textAlign: "center",
		width: "100%",
		color: "dimgrey",
		fontSize: "1.7rem",
		textAlign: "center",
		[theme.breakpoints.down("xs")]: {
			fontSize: "1.5rem",
		},
	},
	orderNum: {
		marginTop: "0.8rem",
		fontWeight: 500,
		textAlign: "center",
		width: "100%",
		fontSize: "1.8rem",
		textAlign: "center",
		[theme.breakpoints.down("xs")]: {
			fontSize: "1.5rem",
		},
	},
	bankBox: {
		display: "flex",
		flexDirection: "row",
		alignItems: "center",
	},
	bankBoxImg: {
		borderRadius: "50%",
		overflow: "hidden",
		width: "46px",
		height: "46px",
		[theme.breakpoints.down("xs")]: {
			width: "30px",
			height: "30px",
		},
	},
	bankBoxText: {
		fontSize: "1.2rem",
		marginLeft: "1rem",
		[theme.breakpoints.down("xs")]: {
			fontSize: "1.1rem",
		},
	},
	payButton: {
		height: "40px",
	},
	dateHolder: {
		display: "flex",
		flexDirection: "column",
		width: "30%",
	},
	dateHolderTitle: {
		fontSize: "1.2rem",
		[theme.breakpoints.down("xs")]: {
			fontSize: "0.9rem",
		},
	},
	dateHolderContent: {
		backgroundColor: "white",
		padding: "0.8rem 0.5rem",
		borderRadius: "7px",
		display: "flex",
		alignItems: "center",
		justifyContent: "center",
	},
	dateHolderText: {
		fontSize: "1.5rem",
		[theme.breakpoints.down("xs")]: {
			fontSize: "1rem",
		},
	},
	cartListBox: {
		width: "100%",
		margin: "3rem 0 5rem",
		//padding: "0 2rem",
		boxSizing: "border-box",
		display: "flex",
		flexDirection: "column",
		[theme.breakpoints.down("xs")]: {
			margin: "1rem 0 3rem",
		},
	},
	itemList: {
		width: "100%",
		display: "flex",
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-between",
		padding: "1rem 0",
	},
	itemListImgContainer: {
		width: "15%",
		overflow: "hidden",
	},
	itemListContent: {
		width: "80%",
		display: "flex",
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-between",
	},
	itemListContentLeft: {
		display: "flex",
		flexDirection: "column",
	},
	itemListContentTitle: {
		fontSize: "19rem",
		fontWeight: 500,
		[theme.breakpoints.down("xs")]: {
			fontSize: "1.4rem",
		},
	},

	itemListContentRight: {
		display: "flex",
		flexDirection: "column",
		alignItems: "flex-end",
	},
	itemListContentPrice: {
		fontSize: "1.7rem",
		[theme.breakpoints.down("xs")]: {
			fontSize: "1.2rem",
		},
	},
	redirectText: {
		fontSize: "1.5rem",
		marginTop: "3rem",
	},
	button: {
		margin: "0.5rem 0",
		fontSize: "1.1rem",
	},
	backIconContainer: {
		alignSelf: "flex-end",
		transition: "all 0.3s",
		cursor: "pointer",
		boxShadow:
			"-5px -5px 5px rgba(255, 255, 255, 0.5), 5px 5px 10px rgba(174, 174, 192, 0.5), inset -2px -2px 4px rgba(0, 0, 0, 0.1), inset 2px 2px 4px #FFFFFF",
		// boxShadow: "2px 2px 4px #e3eeff, -2px -2px 4px #ffffff",
		display: "flex",
		justifyContent: "center",
		alignItems: "center",
		height: "40px",
		width: "40px",
		borderRadius: "50%",
		background: "linear-gradient(145deg, #fcfdff, #dbdbe8)",
		"&:active": {
			background: "linear-gradient(145deg, #dbdbe8, #fcfdff)",
		},
	},
	backIcon: {
		color: greydark,
		fontSize: "2rem",
	},
	receiptModalHeader: {
		display: "flex",
		flexDirection: "row",
		alignItems: "flex-end",
		justifyContent: "space-between",
		margin: "2.5rem 0 1.5rem",
	},
	receiptModalTitle: {
		fontSize: "2.3rem",
		fontWeight: 500,
		paddingRight: "1.5rem",
	},
	downloadIcon: {
		fontSize: "2.8rem",
		cursor: "pointer",
		color: "#252525",
		paddingBottom: "0.5rem",
	},
	emailSection: {
		width: "100%",
		margin: "2rem 0",
		boxSizing: "border-box",
		padding: "0 1rem",
	},
	bankInfoBox: {
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		border: "1px solid gainsboro",
		borderRadius: "10px",
		boxSizing: "border-box",
		padding: "1rem 0.5rem",
		margin: "2.5rem 0",
		[theme.breakpoints.down("xs")]: {
			margin: "1.5rem 0",
		},
	},
	bankInfoTitle: {
		textAlign: "center",
		fontSize: "1.5rem",
		fontWeight: 300,
		marginBottom: "1rem",
		[theme.breakpoints.down("xs")]: {
			fontSize: "1.1rem",
		},
	},
	bankInfoDesc: {
		textAlign: "center",
		fontSize: "1.3rem",
		letterSpacing: "0.2px",
		marginBottom: "0.3rem",
		color: greydark,
		[theme.breakpoints.down("xs")]: {
			fontSize: "1.1rem",
		},
	},
	DialogRoot: {
		backgroundColor: "rgba(0,0,0,0)",
		boxShadow: "none",
	},
	receiptPaperScrollPaper: {
		maxHeight: "none",
		height: "100vh",
		minHeight: "500px",
		backgroundColor: mainBgColor,
	},
	headerNavText: {
		alignSelf: "flex-end",
		padding: "1rem",
		fontSize: "1.5rem",
		cursor: "pointer",
		color: secondary,
	},
});

class Order extends Component {
	state = {
		itemDialog: false,
		paid: null,
		isBillObj: false,
		receiptModal: false,
		email: "",
		isEmail: false,
		pMethod: "",
	};

	componentDidMount() {
		this.props.setLoading(true);

		let urlParams = new URLSearchParams(`?${window.location.href.split("?")[1]}`);

		let isShow = urlParams.get("show");
		if (isShow) {
			this.setState({
				receiptModal: eval(isShow),
			});
		}

		if (!isEmpty(this.props.match.params.oID)) {
			this.props.getBookDetails(this.props.match.params.oID, this.props.business.detail.id);

			let paid = urlParams.get("billplz[paid]");

			if (!isEmpty(paid)) {
				this.setState({
					paid: eval(paid),
					isBillObj: true,
					pMethod: 1,
				});
			}
		} else {
			this.handleBack();
		}
	}

	//   componentWillReceiveProps(nextProps) {
	//     if (!isEmpty(nextProps.loading.status)) {
	//       this.setState({
	//         loading: nextProps.loading.status,
	//       });
	//     }
	//   }

	componentDidUpdate(prevProps, prevState) {
		if (!prevProps.booking.isBookingFetched && this.props.booking.isBookingFetched) {
			this.props.setLoading(false);
			if (!isEmpty(this.props.booking.bookDetails)) {
				this.props.getBankDetails(this.props.booking.bookDetails.business.id);
				if (!this.state.isBillObj) {
					this.setState({
						paid:
							this.props.booking.bookDetails.status !== "rejected" &&
							(this.props.booking.bookDetails.status !== "unpaid" ||
								(this.props.booking.bookPayment &&
									this.props.booking.bookPayment.payment_method_id != 1 &&
									this.props.booking.bookPayment.payment_method_id != null)),
						pMethod: this.props.booking.bookPayment
							? this.props.booking.bookPayment.payment_method_id
							: this.state.pMethod,
						isEmail: !isEmpty(this.props.booking.bookDetails.customer.email),
						email: this.props.booking.bookDetails.customer.email,
					});
				}
			}
		}
	}

	handleBack = () => {
		window.location.href = `https://${this.props.booking.bookDetails.business.subdomain}.storeup.site`;
	};

	handleMail = () => {
		window.location.href = "mailto:support@mail.pinjam.co";
	};

	handleReceiptModal = state => () => {
		this.setState({
			receiptModal: state,
		});
	};

	handleReceipt = async () => {
		this.props.setLoading(true);
		window.scrollTo(0, 0);
		setTimeout(() => {
			var sheets = document.getElementById("dbotReceipt");
			html2canvas(sheets, { useCORS: true }).then(canvas => {
				var srcImg = canvas;
				var sX = 0;
				var sY = 0;
				var sWidth = canvas.width;
				var sHeight = canvas.height;
				var dX = 0;
				var dY = 0;
				var dWidth = canvas.width;
				var dHeight = canvas.height;

				var onePageCanvas = document.createElement("canvas");
				onePageCanvas.setAttribute("width", sWidth);
				onePageCanvas.setAttribute("height", sHeight);
				var ctx = onePageCanvas.getContext("2d");

				ctx.drawImage(srcImg, sX, sY, sWidth, sHeight, dX, dY, dWidth, dHeight);

				const img = onePageCanvas.toDataURL("image/jpeg", 1);

				var link = document.createElement("a");
				link.download = `StoreUp-#${this.props.booking.bookDetails.ref}-Receipt.jpg`;
				link.href = img;
				link.click();
			});
			this.props.setLoading(false);
		}, 1000);
	};

	handleCopy = text => () => {
		navigator.clipboard.writeText(text).then(
			() => {
				this.props.customNotification("Text copied", "success");
			},
			() => {
				this.props.customNotification("Error copying text", "error");
			}
		);
	};

	handleWhatsapp = () => {
		// let text = `[  _Order ID:_ *${this.props.booking.bookDetails.order_id}*  ]`;

		let text = `Hi ${this.props.booking.bookDetails.business.name}!\n\nI'm ${this.props.booking.bookDetails.customer.name}, and this is my order ID: *${this.props.booking.bookDetails.ref}*.`;
		window.open(
			`https://wa.me/60${this.props.booking.bookDetails.business.phone}?text=${encodeURIComponent(text)}`
		);
	};

	render() {
		const { classes } = this.props;
		const { isBillObj, paid, receiptModal, email, isEmail, pMethod } = this.state;
		const { bookDetails, bookShipping, bookPayment, bookBank, isBookingFetched } = this.props.booking;
		const { detail } = this.props.business;

		// const loading = this.props.loading.status;

		if (isBookingFetched) {
			if (!isEmpty(bookDetails)) {
				return (
					<div className={classes.root}>
						<PageHelmet
							metadata={{
								title: `Order details for ${this.props.match.params.oID} | ${
									!isEmpty(detail) ? `${detail.name}` : "StoreUp"
								}`,
							}}
						/>
						<div
							style={{
								width: "100%",
								display: "flex",
								flexDirection: "row",
								justifyContent: "space-between",
								fontSize: "1.2rem",
							}}
						>
							<div
								className={classes.headerNavText}
								style={{
									display: "flex",
									flexDirection: "row",
									justifyContent: "center",
									alignItems: "center",
								}}
								onClick={this.handleBack}
							>
								<ChevronLeftRounded style={{ fontSize: "2rem" }} />
								<Typography style={{ fontSize: "1.5rem" }}>Back to Home</Typography>
							</div>
							<Typography onClick={this.handleReceipt} className={classes.headerNavText}>
								Download
							</Typography>
						</div>
						{/* <Dialog
              fullScreen
              open={receiptModal}
              onClose={this.handleReceiptModal(false)}
              classes={{
                paperScrollPaper: classes.receiptPaperScrollPaper,
              }}
            >
              <div
                style={{
                  width: "90%",
                  margin: "0 auto",
                  maxWidth: "600px",
                  marginBottom: isMobileSafari ? "1.5rem" : 0,
                  padding: "4rem 0 3.5rem",
                  alignSelf: "center",
                  display: "flex",
                  flexDirection: "column",
                }}
              >
                <div
                  className={classes.backIconContainer}
                  onClick={this.handleReceiptModal(false)}
                >
                  <CloseRounded className={classes.backIcon} />
                </div>
                <div className={classes.receiptModalHeader}>
                  <Typography className={classes.receiptModalTitle}>
                    Here's your payment details
                  </Typography>
                  <SystemUpdateAltRounded
                    onClick={this.handleReceipt}
                    className={classes.downloadIcon}
                  />
                </div>
                <Receipt />
              </div>
            </Dialog> */}
						{!isEmpty(paid) ? (
							paid ? (
								<div className={classes.contentSucess}>
									<CheckIcon className={classes.statusIcon} style={{ color: "#1c9c91" }} />
									<Typography className={classes.welcomeText}>
										Thank you! Your order has been placed.
									</Typography>
									{bookPayment &&
									!bookPayment.paid &&
									bookPayment.payment_method_id === 2 &&
									!isEmpty(bookBank.bank_acc) ? (
										<div className={classes.bankInfoBox}>
											<Typography className={classes.bankInfoTitle}>
												Please transfer{" "}
												<span style={{ fontWeight: 700 }}>
													RM{bookPayment.amount.toFixed(2)}
												</span>{" "}
												to the following bank account:
											</Typography>
											<Typography className={classes.bankInfoDesc}>
												{bookBank.acc_name}
											</Typography>
											<Typography className={classes.bankInfoDesc}>
												{ListBank.find(f => f.code === bookBank.bank_code).name}
											</Typography>
											<div
												style={{
													display: "flex",
													flexDirection: "row",
													alignItems: "center",
													justifyContent: "center",
												}}
											>
												<Typography
													className={classes.bankInfoDesc}
													style={{ marginBottom: 0 }}
												>
													{bookBank.bank_acc}
												</Typography>
												<div
													style={{
														backgroundColor: "rgba(234,234,229)",
														padding: "0.4rem 0.6rem",
														borderRadius: "10px",
														marginLeft: "0.5rem",
														cursor: "pointer",
													}}
												>
													<Typography
														style={{
															lineHeight: "0.8rem",
															fontSize: "0.9rem",
															color: secondary,
														}}
														onClick={this.handleCopy(bookBank.bank_acc)}
													>
														Copy
													</Typography>
												</div>
											</div>
											<Button
												color="primary"
												variant="contained"
												onClick={this.handleWhatsapp}
												className={classes.button}
												style={{ marginTop: "1rem", width: "90%" }}
											>
												Inform seller
											</Button>
										</div>
									) : (
										<div style={{ marginBottom: "2rem" }} />
									)}

									{/* <Button
                    color="secondary"
                    variant="outlined"
                    onClick={this.handleBack}
                    className={classes.button}
                  >
                    Back to homepage
                  </Button>
                  {!isEmpty(bookDetails) ? (
                    bookDetails.status === 1 ||
                    bookDetails.status === 2 ||
                    bookDetails.status === 3 ? (
                      <Button
                        color="secondary"
                        variant="contained"
                        onClick={this.handleReceiptModal(true)}
                        className={classes.button}
                      >
                        Payment details
                      </Button>
                    ) : (
                      <Button
                        color="secondary"
                        variant="contained"
                        onClick={this.handleWhatsapp}
                        className={classes.button}
                      >
                        Whatsapp seller
                      </Button>
                    )
                  ) : null} */}

									{/* <Typography className={classes.welcomeText}>
            Here is your order reference number
          </Typography>
          <Typography
            className={classes.orderNum}
            style={{ color: "black" }}
          >
            #{this.props.router.query.bID}
          </Typography> */}
								</div>
							) : (
								<div className={classes.contentSucess}>
									<HighlightOffRounded
										className={classes.statusIcon}
										style={{ color: "firebrick" }}
									/>
									<Typography className={classes.welcomeText}>
										Oppps! Your payment is not complete.
									</Typography>
									<div style={{ marginBottom: "2rem" }} />
									{/* <Button
                    color="secondary"
                    variant="outlined"
                    onClick={this.handleBack}
                    className={classes.button}
                  >
                    Back to homepage
                  </Button> */}
								</div>
							)
						) : null}
						{/* {paid && ( */}
						{/* <Fade in={false}> */}
						<div
							id="dbotReceipt"
							style={{
								width: "600px",
								display: "flex",
								justifyContent: "center",
								position: "absolute",
								top: 0,
								zIndex: -999,
							}}
						>
							<div
								style={{
									width: "100%",
									alignSelf: "center",
									padding: "1.5rem 0",
								}}
							>
								<Receipt toPrint />
							</div>
						</div>
						{/* </Fade> */}
						{/* )} */}
						<Receipt />
					</div>
				);
			} else {
				return <NotFound />;
			}
		} else {
			return <div />;
		}
	}
}

const mapStateToProps = state => ({
	auth: state.auth,
	business: state.business,
	booking: state.booking,
	item: state.item,
	//   loading: state.loading,
	error: state.error,
});

export default connect(mapStateToProps, {
	getBookDetails,
	getBankDetails,
	submitCustomerEmail,
	setLoading,
	customNotification,
})(withStyles(styles)(withRouter(Order)));
