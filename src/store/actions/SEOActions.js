import axios from "axios";
import { APILINK } from "../../utils/key";
import { getError } from "./errorAction";

export const getSEOBusiness = async (subdomain) => {
  let data = {};
  await axios
    .get(`${APILINK}/v1/seo/business/${subdomain}`)
    .then(async (res) => {
      data = res.data.business;
    })
    .catch((err) => {
      getError(err);
    });

  return data;
};

export const getSEOProduct = async (id) => {
  let data = {};
  await axios
    .get(`${APILINK}/v1/seo/item/${id}`)
    .then(async (res) => {
      console.log(res.data);
      data = res.data.item;
    })
    .catch((err) => {
      getError(err);
    });

  return data;
};
