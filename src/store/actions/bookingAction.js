import axios from "axios";

import {
	GET_BOOK_DETAILS,
	GET_BOOK_RECEIPT,
	GET_PAYOUT_DETAILS,
	GET_BANK,
	GET_GUEST,
	BILL_LINK,
	GET_BOOKING_ID,
	FPX_BANK,
	GET_WHATSAPP_DETAILS,
} from "./index";
import { setLoading } from "./loadingAction";
import { APILINK, XANO_APILINK } from "../../utils/key";
import isEmpty from "../../utils/isEmpty";
import { clearCart, getCartItem } from "./itemAction";
import { clearNotification, customNotification } from "./notificationAction";
import { getError, setInputError } from "./errorAction";
import { endSession } from "./customerAction";

export const getCustomerAccount =
	(data = null) =>
	dispatch => {
		dispatch({
			type: GET_GUEST,
			payload: { token: "", details: {} },
		});
		if (!isEmpty(data)) {
			dispatch(setLoading(true));
			axios
				.get(`${APILINK}/v2/auth/guest`, { params: data })
				.then(res => {
					dispatch({
						type: GET_GUEST,
						payload: res.data,
					});
					setTimeout(() => {
						dispatch(setLoading(false));
					}, 700);
				})
				.catch(err => {
					dispatch(setLoading(false));
					dispatch(getError(err));
				});
		}
	};

// export const createOrder = (data) => (dispatch) => {
//   dispatch(setInputError());
//   dispatch(clearNotification());
//   dispatch(setLoading(true));
//   axios
//     .post(
//       `${APILINK}/v1/order?session_id=${sessionStorage.getItem("sessionID")}`,
//       data
//     )
//     .then(async (res) => {
//       dispatch(clearNotification());
//       dispatch(endSession());
//       if (!isEmpty(res.data.payment_link)) {
//         window.location.href = `${res.data.payment_link}?auto_submit=true`;
//       } else {
//         let host = window.location.hostname;
//         let isProd = host.includes("storeup.site");
//         let bizID = isProd ? host.split(".storeup.site")[0] : "inonity";
//         window.location.href = `https://${bizID}.storeup.site/o/${res.data.order_id}`;
//       }
//       // dispatch({
//       //   type: GET_BOOKING_ID,
//       //   payload: res.data.payment_link,
//       // });
//     })
//     .catch((err) => {
//       dispatch(setLoading(false));
//       dispatch(getError(err));
//     });
// };

export const completeOrder = (data, oID, sID) => dispatch => {
	dispatch(setInputError());
	dispatch(clearNotification());
	dispatch(setLoading(true));
	axios
		.patch(`${APILINK}/v1/order/partial/${oID}?session_id=${sID}`, data)
		.then(async res => {
			dispatch(clearNotification());
			dispatch(endSession());
			if (!isEmpty(res.data.payment_link)) {
				window.location.href = `${res.data.payment_link}?auto_submit=true`;
			} else {
				let host = window.location.hostname;
				let isProd = host.includes("storeup.site");
				let bizID = isProd ? host.split(".storeup.site")[0] : "inonity";
				window.location.href = `https://${bizID}.storeup.site/o/${res.data.order_id}`;
			}
		})
		.catch(err => {
			dispatch(setLoading(false));
			dispatch(getError(err));
		});
};

export const createLead = data => dispatch => {
	dispatch(setInputError());
	dispatch(clearNotification());
	dispatch(setLoading(true));

	axios
		.post(`${APILINK}/v1/order/partial?session_id=${sessionStorage.getItem("sessionID")}`, data)
		.then(async res => {
			dispatch(clearNotification());

			dispatch(endSession());
			dispatch({
				type: GET_WHATSAPP_DETAILS,
				payload: res.data.order_id,
				phone: res.data.contact_number,
			});
		})
		.catch(err => {
			dispatch(setLoading(false));
			dispatch(getError(err));
		});
};

export const getPaymentList = () => dispatch => {
	axios
		.get(`https://api.pinjam.co/fpx-bank`)
		.then(res => {
			dispatch({
				type: FPX_BANK,
				payload: res.data.banks,
			});
		})
		.catch(err => {
			dispatch(getError(err));
		});
};

export const payBooking = (data, ref, payNum) => dispatch => {
	dispatch(setLoading(true));
	axios
		.post(`${APILINK}/v2/booking/${ref}/payment/online/${payNum}`, data)
		.then(res => {
			dispatch({
				type: BILL_LINK,
				payload: res.data.url,
			});
		})
		.catch(err => {
			dispatch(setLoading(false));
			dispatch(getError(err));
		});
};

export const getBookDetails = (id, bID) => dispatch => {
	if (id === null) {
		dispatch({
			type: GET_BOOK_DETAILS,
			payload: {},
		});
	} else {
		dispatch(setLoading(true));
		axios
			.get(`${XANO_APILINK}/order/${id}`)
			.then(res => {
				dispatch(setLoading(false));

				dispatch({
					type: GET_BOOK_DETAILS,
					payload: res.data.order,
					isFetch: true,
				});
			})
			.catch(err => {
				dispatch(setLoading(false));
				dispatch({
					type: GET_BOOK_DETAILS,
					payload: {},
					isFetch: true,
				});
				dispatch(getError(err));
			});
	}
};

export const getBankDetails = id => dispatch => {
	axios
		.get(`${XANO_APILINK}/business/${id}/bank`)
		.then(res => {
			dispatch({
				type: GET_PAYOUT_DETAILS,
				payload: res.data.business_bank || {
					acc_name: "",
					bank_code: "",
					bank_acc: "",
					id_type: "",
					id_number: "",
					verified: "",
				},
			});
		})
		.catch(async err => {
			dispatch(getError(err));
		});
};

export const submitCustomerEmail = (ref, bID, data) => dispatch => {
	dispatch(setLoading(true));
	axios
		.patch(`${APILINK}/v1/order/${ref}/customer/email?business_id=${bID}`, data)
		.then(res => {
			dispatch(customNotification(res.data.message, "success"));
			dispatch(setLoading(false));
		})
		.catch(err => {
			dispatch(setLoading(false));
			dispatch(getError(err));
		});
};

export const getBookReceipt = id => dispatch => {
	if (id === null) {
		dispatch({
			type: GET_BOOK_RECEIPT,
			payload: {},
		});
	} else {
		dispatch(setLoading(true));
		axios
			.get(`${APILINK}/v2/booking/${id}/receipt`)
			.then(res => {
				dispatch(setLoading(false));

				dispatch({
					type: GET_BOOK_RECEIPT,
					payload: res.data,
				});
			})
			.catch(err => {
				dispatch(setLoading(false));
				dispatch({
					type: GET_BOOK_RECEIPT,
					payload: {},
				});
				dispatch(getError(err));
			});
	}
};
