import axios from "axios";
import { APILINK, XANO_APILINK } from "../../utils/key";
import { setLoading } from "./loadingAction";

import { GET_BUSINESS, GET_BUSINESS_LISTING, GET_CUSTOM_FIELD } from "./index";
import { getError } from "./errorAction";
import isEmpty from "../../utils/isEmpty";

export const getAllStoreWithListings = async () => {
	let data = [];
	await axios
		.get(`${APILINK}/v2/business/directory/sitemap`)
		.then(res => {
			data = res.data.result;
		})
		.catch(err => {
			getError(err);
		});
	return data;
};

export const getBusinessIDbySubdomain = name => async dispatch => {
	let val = "";

	if (name) {
		dispatch(setLoading(true));
		await axios
			.get(`${XANO_APILINK}/business?subdomain=${name}`)
			.then(res => {
				if (!isEmpty(res.data)) {
					val = res.data.id;
				}
				dispatch(setLoading(false));
			})
			.catch(err => {
				dispatch({
					type: GET_BUSINESS,
					payload: {},
				});
				dispatch(setLoading(false));
				dispatch(getError(err, true));
			});
	} else {
		dispatch({
			type: GET_BUSINESS,
			payload: {},
		});
	}

	return val;
};

export const getBusiness = id => dispatch => {
	dispatch(setLoading(true));
	axios
		.get(`${APILINK}/v1/business/info/id/${id}`)
		.then(res => {
			dispatch({
				type: GET_BUSINESS,
				payload: res.data.business,
			});
			dispatch(setLoading(false));
		})
		.catch(err => {
			dispatch(setLoading(false));
			dispatch(getError(err));
		});
};

export const getBusinessCatalogItem = (name, bID, sID) => dispatch => {
	dispatch(setLoading(true));
	axios
		.get(`${APILINK}/v1/catalog/business/${bID}/name/${name}/items?session_id=${sID}`)
		.then(res => {
			dispatch({
				type: GET_BUSINESS_LISTING,
				payload: res.data.items,
			});
			dispatch(setLoading(false));
		})
		.catch(err => {
			dispatch(setLoading(false));
			dispatch(getError(err));
		});
};

export const getBusinessCustomField = id => dispatch => {
	axios
		.get(`${APILINK}/v2/business/${id}/custom-field`)
		.then(res => {
			dispatch({
				type: GET_CUSTOM_FIELD,
				payload: res.data.fields,
			});
		})
		.catch(err => {
			dispatch(getError(err));
		});
};
