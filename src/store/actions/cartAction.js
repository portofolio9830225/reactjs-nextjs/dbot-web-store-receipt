import axios from "axios";
import { APILINK } from "../../utils/key";

import {
  GET_ALL_CATALOG,
  GET_CART_BREAKDOWN,
  SET_CART_ITEM,
  GET_CATALOG_SHIPPING,
  GET_PAYMENT_METHOD,
} from "./index";
import { getError } from "./errorAction";
import isEmpty from "../../utils/isEmpty";
import { clearNotification } from "./notificationAction";
import { setLoading } from "./loadingAction";

export const getAllCatalog = (bID) => (dispatch) => {
  axios
    .get(`${APILINK}/v1/catalog/business/${bID}`)
    .then((res) => {
      dispatch({
        type: GET_ALL_CATALOG,
        payload: [],
      });
      dispatch({
        type: GET_ALL_CATALOG,
        payload: res.data.catalogs,
      });
    })
    .catch((err) => {
      dispatch(setLoading(false));
      dispatch(getError(err));
    });
};

export const setCartItem =
  (cart, catID, bID, data = null) =>
  (dispatch) => {
    let arr = [...cart];

    if (!isEmpty(data)) {
      let foundIndex = arr.findIndex((e) => e.item_id === data.item_id);
      if (data.quantity > 0) {
        if (foundIndex !== -1) {
          arr[foundIndex] = data;
        } else {
          arr.push(data);
        }
      } else {
        if (foundIndex !== -1) {
          arr.splice(foundIndex, 1);
        }
      }
    }

    if (!isEmpty(arr)) {
      dispatch(
        getCartBreakdown({ catalog_id: catID, items: arr, business_id: bID })
      );
    } else {
      dispatch(getCartBreakdown());
    }

    dispatch({
      type: SET_CART_ITEM,
      payload: arr,
    });
  };

export const getCartBreakdown =
  (data = null) =>
  (dispatch) => {
    if (isEmpty(data)) {
      dispatch({
        type: GET_CART_BREAKDOWN,
        payload: {},
      });
    } else {
      axios
        .post(`${APILINK}/v1/order/price/breakdown`, data)
        .then((res) => {
          dispatch({
            type: GET_CART_BREAKDOWN,
            payload: {},
          });
          dispatch({
            type: GET_CART_BREAKDOWN,
            payload: res.data,
          });
        })
        .catch((err) => {
          dispatch(getError(err));
        });
    }
  };

export const getShippingCatalog = (catID, bID) => (dispatch) => {
  axios
    .get(`${APILINK}/v1/shipping?business_id=${bID}&catalog_id=${catID}`)
    .then((res) => {
      dispatch({
        type: GET_CATALOG_SHIPPING,
        payload: res.data,
      });
    })
    .catch((err) => {
      dispatch(getError(err));
    });
};

// export const getShippingCatalog = (catID, bID) => (dispatch) => {
//   axios
//     .get(`${APILINK}/v1/shipping/catalog/${catID}?business_id=${bID}`)
//     .then((res) => {
//       dispatch({
//         type: GET_CATALOG_SHIPPING,
//         payload: res.data,
//       });
//     })
//     .catch((err) => {
//       dispatch(getError(err));
//     });
// };

// export const getShippingItem = (bID) => (dispatch) => {
//   axios
//     .get(`${APILINK}/v1/shipping/template/business/${bID}`)
//     .then((res) => {
//       dispatch({
//         type: GET_CATALOG_SHIPPING,
//         payload: {
//           is_zone_based: 0,
//           is_weight_based: 0,
//           is_product_based: 1,
//           shipping: { product: res.data.templates },
//         },
//       });
//     })
//     .catch((err) => {
//       dispatch(getError(err));
//     });
// };

export const getPaymentMethod = (id) => (dispatch) => {
  axios
    .get(`${APILINK}/v1/catalog/${id}/payment-method`)
    .then((res) => {
      dispatch({
        type: GET_PAYMENT_METHOD,
        payload: res.data.payment_method,
      });
    })
    .catch((err) => {
      dispatch(getError(err));
    });
};
