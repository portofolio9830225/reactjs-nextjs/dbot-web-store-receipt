import axios from "axios";
import isEmpty from "../../utils/isEmpty";
import { APILINK } from "../../utils/key";
import { setLoading } from "./loadingAction";
import { getError } from "./errorAction";

import {
  GET_ITEM,
  CLEAR_ITEM_STATE,
  GET_ITEM_NA,
  GET_CART_ITEMS,
  DELETE_CART,
  CLEAR_CART,
  SET_CATEGORY,
} from "./index";
import { clearNotification, customNotification } from "./notificationAction";

export const clearItem = () => (dispatch) => {
  dispatch({
    type: CLEAR_ITEM_STATE,
  });
};

export const getItemDetails = (itmid, bID) => (dispatch) => {
  dispatch(setLoading(true));
  axios
    .get(
      `${APILINK}/v1/item/${itmid}/public?business_id=${bID}&session_id=${sessionStorage.getItem(
        "sessionID"
      )}`
    )
    .then((res) => {
      dispatch(clearItem());
      dispatch({
        type: GET_ITEM,
        payload: res.data,
      });
      dispatch(setLoading(false));
    })
    .catch((err) => {
      dispatch({
        type: GET_ITEM,
        payload: {},
      });
      dispatch(setLoading(false));
      dispatch(getError(err));
    });
};

export const getCartItem = (id) => (dispatch) => {
  axios
    .get(`${APILINK}/v2/cart/${id}?cart_status=1`)
    .then((res) => {
      if (!isEmpty(res.data.cart)) {
        dispatch({
          type: GET_CART_ITEMS,
          payload: { ...res.data.cart, cart_id: id },
          customField: res.data.custom_field,
          conflictItem: res.data.item_not_available,
        });
      } else {
        dispatch({
          type: GET_CART_ITEMS,
          payload: null,
          customField: [],
          conflictItem: [],
        });
      }

      dispatch(setLoading(false));
    })
    .catch((err) => {
      dispatch(setLoading(false));
      dispatch(getError(err));
    });
};

export const clearCart = () => (dispatch) => {
  dispatch({
    type: CLEAR_CART,
    payload: [],
  });
};

export const editCart = (id, data) => (dispatch) => {
  dispatch(setLoading(true));
  dispatch(clearNotification());
  axios
    .patch(`${APILINK}/v2/cart/${id}`, data)
    .then((res) => {
      dispatch(setLoading(false));
      dispatch(getCartItem(id));
    })
    .catch((err) => {
      dispatch(setLoading(false));

      dispatch(getError(err));
    });
};

export const deleteCartItem =
  (cart, item = null) =>
  (dispatch) => {
    dispatch(clearNotification());
    axios
      .delete(
        `${APILINK}/v2/cart/${cart}${!isEmpty(item) ? `/item/${item}` : ""}`
      )
      .then((res) => {
        if (isEmpty(item)) {
          dispatch({
            type: DELETE_CART,
            payload: cart,
          });
        }
        dispatch(customNotification(res.data.message, "success"));
        dispatch(getCartItem(cart));
      })
      .catch((err) => {
        dispatch(setLoading(false));
        dispatch(getError(err));
      });
  };

export const getItemNA = (id) => (dispatch) => {
  axios
    .get(`${APILINK}/v2/item/detail/${id}/not-available`)
    .then((res) => {
      dispatch({
        type: GET_ITEM_NA,
        payload: res.data,
      });
    })
    .catch((err) => {
      dispatch(getError(err));
    });
};

export const setCategory = (id) => (dispatch) => {
  dispatch({
    type: SET_CATEGORY,
    payload: id,
  });
};
