import { SET_LOADING } from "./index";

export const setLoading = (state) => {
  return {
    type: SET_LOADING,
    payload: state,
  };
};
