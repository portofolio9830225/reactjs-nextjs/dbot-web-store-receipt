import { SET_PRODUCT_BOX_TYPE } from "./index";

export const setProductBoxType = (type) => (dispatch) => {
  dispatch({
    type: SET_PRODUCT_BOX_TYPE,
    payload: type,
  });
};
