import isEmpty from "../../utils/isEmpty";
import {
	GET_BOOK_DETAILS,
	GET_BANK,
	FPX_BANK,
	GET_GUEST,
	BILL_LINK,
	GET_BOOK_RECEIPT,
	GET_PAYOUT_DETAILS,
} from "../actions";

const initialState = {
	message: "",
	booking_id: "",
	priceDetails: {},
	bank: [],
	guest: {
		token: "",
		details: {},
	},
	bookDetails: {},
	bookItems: [],
	isBookingFetched: false,
	bookReceipt: {},
	bookPayment: {},
	bookShipping: {},
	bookPromo: {},
	bookBank: {},
	fpx: [],
	billURL: "",
};

export default function (state = initialState, action) {
	switch (action.type) {
		case GET_BANK:
			return {
				...state,
				bank: action.payload,
			};
		case GET_GUEST:
			return {
				...state,
				guest: {
					...state.guest,
					token: action.payload.token,
					details: action.payload.details,
				},
			};
		case GET_PAYOUT_DETAILS:
			return {
				...state,
				bookBank: action.payload,
			};
		case GET_BOOK_DETAILS:
			if (isEmpty(action.payload)) {
				return {
					...state,
					bookDetails: {},
					bookItems: [],
					bookPayment: {},
					bookShipping: {},
					//bookBank: {},
				};
			} else {
				let subtotal = 0;
				action.payload.products.map(p => {
					subtotal += p.price * p.quantity;
				});

				let { products, payment, shipping, promo, ...detail } = action.payload;

				return {
					...state,
					bookDetails: {
						...detail,
						// items_subtotal: subtotal,
					},
					bookItems: products,
					bookPayment: payment,
					bookShipping: shipping,
					isBookingFetched: action.isFetch,
					bookPromo: promo,
					//orderBank: action.payload.business_bank,
				};
			}
		// return {
		// 	...state,
		// 	bookDetails: !isEmpty(action.payload)
		// 		? {
		// 				...action.payload.order,
		// 				items_subtotal: action.payload.items_subtotal,
		// 				addressData: action.payload.address_raw,
		// 		  }
		// 		: {},
		// 	bookItems: !isEmpty(action.payload) ? action.payload.items : [],
		// 	bookPayment: action.payload.payment,
		// 	bookShipping: action.payload.shipping,
		// 	bookBank: !isEmpty(action.payload) ? action.payload.business_bank : {},
		// 	isBookingFetched: action.isFetch,
		// };
		case GET_BOOK_RECEIPT:
			return {
				...state,
				bookReceipt: action.payload.details,
			};
		case FPX_BANK:
			return {
				...state,
				fpx: action.payload,
			};
		case BILL_LINK:
			return {
				...state,
				billURL: action.payload,
			};
		default:
			return state;
	}
}
