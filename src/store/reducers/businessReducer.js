import {
  GET_BUSINESS,
  GET_BUSINESS_LISTING,
  GET_CUSTOM_FIELD,
} from "../actions";

const initialState = {
  message: "",
  detail: {},
  listings: [],
  customField: [],
  isFetched: false,
  isListingFetched: false,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_BUSINESS:
      return {
        ...state,
        detail: action.payload,
        isFetched: true,
      };
    case GET_BUSINESS_LISTING:
      return {
        ...state,
        listings: action.payload,
        isListingFetched: true,
      };
    case GET_CUSTOM_FIELD:
      return {
        ...state,
        customField: action.payload,
      };

    default:
      return state;
  }
}
