import { combineReducers } from "redux";
import itemReducer from "./itemReducer";
import businessReducer from "./businessReducer";
import bookingReducer from "./bookingReducer";
import notificationReducer from "./notificationReducer";
import loadingReducer from "./loadingReducer";
import errorReducer from "./errorReducer";
import customerReducer from "./customerReducer";
import cartReducer from "./cartReducer";
import storeviewReducer from "./storeviewReducer";

export default combineReducers({
  item: itemReducer,
  business: businessReducer,
  booking: bookingReducer,
  customer: customerReducer,
  cart: cartReducer,
  storeview: storeviewReducer,
  loading: loadingReducer,
  notification: notificationReducer,
  error: errorReducer,
});
