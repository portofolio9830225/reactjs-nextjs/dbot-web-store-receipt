import { SET_LOADING } from "../actions";

const initialState = {
  status: false,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_LOADING:
      return {
        ...state,
        status: action.payload,
      };

    default:
      return state;
  }
}
