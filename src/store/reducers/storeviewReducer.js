import { SET_PRODUCT_BOX_TYPE } from "../actions";

const initialState = {
  productBoxType: 1,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_PRODUCT_BOX_TYPE: {
      return {
        ...state,
        productBoxType: action.payload,
      };
    }

    default:
      return state;
  }
}
