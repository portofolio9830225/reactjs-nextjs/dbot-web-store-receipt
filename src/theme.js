import { createTheme } from "@material-ui/core/styles";
import {
	black,
	mainBgColor,
	primary,
	primaryDark,
	primaryLight,
	secondary,
	secondaryDark,
	secondaryLight,
} from "./utils/ColorPicker";

export const theme = createTheme({
	typography: {
		fontFamily: "Inter, Arial, Helvetica, sans-serif",
	},
	palette: {
		primary: {
			// light: "#333333",
			// main: "#252525",
			// dark: "#000000",
			// contrastText: "#ffffff",
			light: primaryLight,
			main: primary,
			dark: primaryDark,
			contrastText: "#fff",
		},
		secondary: {
			light: secondaryLight,
			main: secondary,
			dark: secondaryDark,
			contrastText: "#fff",
		},
	},
	overrides: {
		MuiTooltip: {
			tooltip: {
				fontFamily: "Inter, Arial, Helvetica, sans-serif",
				fontSize: "0.8rem",
				fontWeight: 400,
				letterSpacing: "0.8px",
			},
		},
		MuiDialog: {
			paperFullScreen: {
				height: "auto",
				maxHeight: "80vh",
			},
		},
		MuiBackdrop: {
			root: {
				backgroundColor: "rgba(0,0,0,0.7)",
			},
		},
		MuiExpansionPanelSummary: {
			expandIcon: {
				color: "rgba(0,0,0,0)",
			},
		},
		MuiExpansionPanel: {
			root: {
				"&:before": {
					backgroundColor: "rgba(0,0,0,0)",
				},
			},
		},
		MuiCollapse: {
			root: {
				width: "100%",
			},
			container: {
				width: "100%",
			},
			wrapper: {
				width: "100%",
			},
			wrapperInner: {
				width: "100%",
			},
		},
		MuiTabs: {
			scroller: { padding: "0 0.1rem" },
		},
		MuiList: {
			root: {
				backgroundColor: mainBgColor,
			},
		},
		MuiListItem: {
			root: {
				letterSpacing: "0.1rem",
			},
		},
		MuiBottomNavigationAction: {
			root: {
				color: "rgba(0, 0, 0, 0.25)",
			},
		},
		MuiButton: {
			root: {
				fontFamily: "Inter, Arial, Helvetica, sans-serif",
				letterSpacing: "0.1rem",
				height: "44px",
				borderRadius: "22px",
				fontSize: "1rem",
			},
			outlinedPrimary: {
				borderWidth: "1.5px",
				"&:hover": {
					borderWidth: "1.5px",
				},
			},
			text: {
				padding: "7px 14px",
			},
			textPrimary: {
				"&:hover": {
					backgroundColor: "rgba(0,0,0,0)",
				},
			},
			textSecondary: {
				backgroundColor: "rgba(229,229,234,1)",
				"&:hover": {
					backgroundColor: "rgba(226,226,231,1)",
				},
			},
		},
		MuiOutlinedInput: {
			notchedOutline: {
				transition: "all 0.3s",
			},
		},
	},
});
