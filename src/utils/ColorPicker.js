export const mainBgColor = "rgba(242,242,247,1)";

export const primaryLight = "#6bff96";
export const primary = "#25d366";
export const primaryDark = "#00a038";

export const secondaryLight = "#6aa8ff";
export const secondary = "#007afe";
export const secondaryDark = "#004fcb";

export const greywhite = "gainsboro";
export const greylight = "darkgrey";
export const grey = "grey";
export const greydark = "dimgrey";
export const black = "#252525";

export const successColor = "#206166";
export const errorColor = "firebrick";
