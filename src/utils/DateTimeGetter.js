import DateConverter from "./DateConverter";
import TimeConverter from "./TimeConverter";

export default ({ dt }) => {
  let date = new Date(dt);

  let h = date.getHours();
  let min = date.getMinutes();

  let d = date.getDate();
  let month = date.getMonth() + 1;
  let y = date.getFullYear();

  return `${DateConverter(`${y}-${month}-${d}`)}, ${TimeConverter(
    `${h}:${min}:00`
  )}`;
};
