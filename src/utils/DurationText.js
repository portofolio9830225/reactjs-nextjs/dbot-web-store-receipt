const DurationText = num => {
  return `${num} ${num <= 1 ? "day" : "days"}`;
};

export default DurationText;
