const BankList = [
	{
		name: "Affin Bank Berhad",
		code: "PHBMMYKL",
		bulkName: "AFFIN",
	},
	{
		name: "AGROBANK / BANK PERTANIAN MALAYSIA BERHAD",
		code: "BPMBMYKL",
		bulkName: "AGRO",
	},
	{
		name: "Alliance Bank Malaysia Berhad",
		code: "MFBBMYKL",
		bulkName: "ALLIANCE",
	},
	{
		name: "AL RAJHI BANKING & INVESTMENT CORPORATION (MALAYSIA) BERHAD",
		code: "RJHIMYKL",
		bulkName: "ALRAJHI",
	},
	{
		name: "AmBank (M) Berhad",
		code: "ARBKMYKL",
		bulkName: "AMBANK",
	},
	{
		name: "Bank Islam Malaysia Berhad",
		code: "BIMBMYKL",
		bulkName: "BANK ISLAM",
	},
	{
		name: "Bank Kerjasama Rakyat Malaysia Berhad",
		code: "BKRMMYKL",
		bulkName: "BANK RAKYAT",
	},
	{
		name: "Bank Muamalat (Malaysia) Berhad",
		code: "BMMBMYKL",
		bulkName: "MUAMALAT",
	},
	{
		name: "Bank Simpanan Nasional Berhad",
		code: "BSNAMYK1",
		bulkName: "BSN",
	},
	{
		name: "CIMB Bank Berhad",
		code: "CIBBMYKL",
		bulkName: "CIMB",
	},
	{
		name: "Citibank Berhad",
		code: "CITIMYKL",
		bulkName: "CITIBANK",
	},
	{
		name: "Hong Leong Bank Berhad",
		code: "HLBBMYKL",
		bulkName: "HONG LEONG",
	},
	{
		name: "HSBC Bank Malaysia Berhad",
		code: "HBMBMYKL",
		bulkName: "HSBC",
	},
	{
		name: "Kuwait Finance House",
		code: " KFHOMYKL",
		bulkName: "KUWAIT FH",
	},
	{
		name: "Maybank / Malayan Banking Berhad",
		code: "MBBEMYKL",
		bulkName: "MAYBANK",
	},
	{
		name: "OCBC Bank (Malaysia) Berhad",
		code: "OCBCMYKL",
		bulkName: "OCBC",
	},
	{
		name: "Public Bank Berhad",
		code: "PBBEMYKL",
		bulkName: "PUBLIC BANK",
	},
	{
		name: "RHB Bank Berhad",
		code: "RHBBMYKL",
		bulkName: "RHB",
	},
	{
		name: "Standard Chartered Bank (Malaysia) Berhad",
		code: "SCBLMYKX",
		bulkName: "STANDARD CHARTERED",
	},
	{
		name: "United Overseas Bank (Malaysia) Berhad",
		code: "UOVBMYKL",
		bulkName: "UOB",
	},
];
export default BankList;
