const TimeConverter = (time) => {
  let newTime, hour, minute, ampm, hourInt, minuteInt;

  hour = time.split(":")[0];
  minute = time.split(":")[1];
  hourInt = parseInt(hour);
  minuteInt = parseInt(minute);

  switch (true) {
    case hourInt === 0:
      {
        hour = "12";
        ampm = "A.M.";
      }
      break;
    case hourInt === 12:
      {
        ampm = "P.M.";
      }
      break;
    case hourInt > 12:
      {
        ampm = "P.M.";
        hourInt = hourInt - 12;
        if (hourInt < 10) {
          hour = `0${hourInt}`;
        } else {
          hour = `${hourInt}`;
        }
      }
      break;
    default:
      {
        ampm = "A.M.";
      }
      break;
  }

  if (minuteInt < 10) {
    minute = `0${minuteInt}`;
  } else {
    minute = `${minuteInt}`;
  }

  newTime = `${hour}:${minute} ${ampm}`;
  return newTime;
};

export default TimeConverter;
