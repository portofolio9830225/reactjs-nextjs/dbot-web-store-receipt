import isEmpty from "./isEmpty";

const isJson = (value) => {
  try {
    if (isEmpty(value)) throw new Error("Empty JSON");
    JSON.parse(value);
    return true;
  } catch (e) {
    return false;
  }
};

export default isJson;
